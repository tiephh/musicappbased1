package com.example.baseproject.Ultils

class Contants {
    companion object{
        const val ALBUM_ID = "album_id"
        const val ALBUM_IMAGE = "album_image"
        const val AlBUM_TITLE = "album_title"
        const val ARTIST_ID = "artist_id"
        const val ARTIST_NAME = "artist_name"
        const val ARTIST_IMAGE = "artist_image"
        const val ALBUM_TOTAL_SONG = "total_song"
    }

    object MediaPlayer{
        const val ACTION_MUSIC = "ACTION_MUSIC"
    }
}