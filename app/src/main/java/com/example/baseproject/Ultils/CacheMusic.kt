package com.example.baseproject.Ultils

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.example.core.model.CacheMusicObject
import com.example.core.utils.Constants
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class CacheMusic @Inject constructor(@ApplicationContext private val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    @Throws(SQLiteConstraintException::class)
    fun insertSong(song: CacheMusicObject): Boolean {
        val db = writableDatabase
        val values = ContentValues()
        values.putNull(Constants.RoomDataBase.ID)
        values.put(Constants.RoomDataBase.SOURCE, song.source)
        values.put(Constants.RoomDataBase.TITLE, song.title)

        db.insert(Constants.RoomDataBase.TABLE_CACHE, null, values)

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteSong(id: String): Boolean {
        val db = writableDatabase
        val selection = Constants.RoomDataBase.ID + " LIKE ?"
        val selectionArgs = arrayOf(id)
        db.delete(Constants.RoomDataBase.TABLE_CACHE, selection, selectionArgs)

        return true
    }


    fun readAllSong(): ArrayList<CacheMusicObject> {
        val song = ArrayList<CacheMusicObject>()
        val db = writableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("select * from " + Constants.RoomDataBase.TABLE_CACHE, null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }

        var source: String
        var idSong: Int
        var title: String
        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                source = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.SOURCE))
                idSong = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ID))
                title = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.TITLE))

                song.add(CacheMusicObject(idSong, source, title))
                cursor.moveToNext()
            }
        }
        return song
    }

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "Reader.db"

        private const val SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Constants.RoomDataBase.TABLE_CACHE + " (" + Constants.RoomDataBase.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    Constants.RoomDataBase.SOURCE + " NVARCHAR(200)," +
                    Constants.RoomDataBase.TITLE + " NVARCHAR(200))"

        private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + Constants.RoomDataBase.TABLE_CACHE
    }

}