package com.example.baseproject.ui.personal

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.baseproject.R
import com.example.core.base.BaseViewModel
import com.example.core.model.Users
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.File
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(private val ref: AppPreferences
, @ApplicationContext private val context: Context) : BaseViewModel(){

    val exception = MutableLiveData<String>()

    fun logout(){
        FirebaseAuth.getInstance().signOut()
    }

    fun changeImage(mUri: Uri){
        val reference = FirebaseStorage.getInstance().reference
        val riversRef = reference.child("images/${mUri.lastPathSegment}")
        riversRef.putFile(mUri).addOnSuccessListener {
            riversRef.downloadUrl.addOnSuccessListener { uri ->
                ref.getDatabaseReference(Constants.Firebase.USER).child(ref.getFirebaseUid())
                    .addValueEventListener(object : ValueEventListener{
                        override fun onDataChange(snapshot: DataSnapshot) {
                            val users: Users? = snapshot.getValue(Users::class.java)
                            if (users != null) {
                                users.image = uri.toString()
                                snapshot.ref.setValue(users)
                            }
                        }

                        override fun onCancelled(error: DatabaseError) {
                            exception.postValue(context.getString(R.string.fail))
                        }
                    })
            }.addOnFailureListener {
                exception.postValue(context.getString(R.string.fail))
            }
        }.addOnFailureListener {
            exception.postValue(context.getString(R.string.fail))
        }
    }
}