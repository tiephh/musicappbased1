package com.example.baseproject.ui.genrees



import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.baseproject.R
import com.example.baseproject.Ultils.Contants
import com.example.core.base.BaseViewModel
import com.example.core.database.PlaylistHelper
import com.example.core.model.AlbumDetail
import com.example.core.model.MediaPlayerSong
import com.example.core.model.Song
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GenresViewModel @Inject constructor(
    val savedStateHandle: SavedStateHandle,
    private val repository: GenresRepository,
    private val appPreferences: AppPreferences,
    private val dataPlaylist: PlaylistHelper,
    @ApplicationContext val context: Context
) : BaseViewModel() {

    val dataName = MutableLiveData<String>()
    val dataImage = MutableLiveData<String>()
    val dataList = MutableLiveData<ArrayList<AlbumDetail>>()
    val dataSetup = MutableLiveData<ArrayList<MediaPlayerSong>>()
    val loading = MutableLiveData<Boolean>()
    val exception = MutableLiveData<String?>()
    var id: Int? = null
    private val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }
    init {
        getBundle()
        getData()
    }

    private fun getData() {
        val keyGenres: String? = savedStateHandle.get(Contants.ALBUM_ID)
        viewModelScope.launch(Dispatchers.IO + handler){
            loading.postValue(true)
            val response = async { repository.getDataGenresByKey() }
            val responseSinger = async { repository.getDataSinger() }
            var singerName: String? = ""
            for (i in responseSinger.await().indices) {
                if (responseSinger.await()[i].id.toString() == keyGenres) {
                    singerName = responseSinger.await()[i].name
                    id = responseSinger.await()[i].id
                }
            }
            val array: ArrayList<AlbumDetail> = ArrayList()
            for (i in response.await().indices) {
                if (response.await()[i].albumId == keyGenres?.toInt()) {
                    array.add(
                        AlbumDetail(
                            response.await()[i].image,
                            singerName,
                            response.await()[i].title,
                            response.await()[i].source,
                            response.await()[i].duration
                        )
                    )
                }
            }
            val listSetup: ArrayList<MediaPlayerSong> = ArrayList()
            for (i in array.indices){
                val song = MediaPlayerSong(
                    1,
                    array[i].title,
                    array[i].image,
                    array[i].singer,
                    array[i].song,
                    array[i].duration!! * 1000
                )
                listSetup.add(song)
            }
            dataSetup.postValue(listSetup)
            dataList.postValue(array)
            loading.postValue(false)
        }
    }

    private fun getBundle() {
        dataName.postValue(savedStateHandle.get(Contants.AlBUM_TITLE))
        dataImage.postValue(savedStateHandle.get(Contants.ALBUM_IMAGE))
    }

    fun insertData(data: AlbumDetail?){
        var isCheck = false
        if(dataPlaylist.readAllSong().size > 0){
            for (i in dataPlaylist.readAllSong().indices){
                if (dataPlaylist.readAllSong()[i].source?.equals(data?.song)!!){
                    isCheck = true
                }
            }
            if (!isCheck){
                dataPlaylist.insertSong(
                    Song(data?.song, data?.image, null,data?.title
                        , null, id,null, data?.duration!! * 1000)
                )
                exception.postValue(context.getString(R.string.success))
            }else{
                exception.postValue(context.getText(R.string.existed).toString())
            }
        }else {
            dataPlaylist.insertSong(
                Song(
                    data?.song, data?.image, null, data?.title, null, id,
                    null, data?.duration!! *1000
                )
            )
            exception.postValue(context.getString(R.string.success))
        }
    }

    fun insertFav(song: MediaPlayerSong) {
        if (FirebaseAuth.getInstance().currentUser != null){
            appPreferences.getDatabaseReference(Constants.Firebase.FAVOURITE).child(appPreferences
                .getFirebaseUid()).addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    val listSong = ArrayList<MediaPlayerSong>()
                    for (dataSnapshot in snapshot.children){
                        dataSnapshot.getValue(MediaPlayerSong::class.java)?.let { listSong.add(it) }
                    }
                    if (!listSong.contains(song)){
                        appPreferences.setSongFirebase(song)
                        exception.postValue(context.getString(R.string.success))
                    }else{
                        exception.postValue(context.getString(R.string.faved))
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    exception.postValue(error.toString())
                }
            })
        }else{
            exception.postValue(context.getString(R.string.login))
        }
    }
}