package com.example.baseproject.ui.tabHomePage

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.core.utils.listenner.OnItemClick
import com.example.baseproject.databinding.ItemAlbumBinding
import com.example.core.model.Album


class SongAdapter: ListAdapter<Album, SongHolder>(SongDiffUtil()){

    private var listener: OnItemClick? = null

    fun setOnItemClickListener(listener: OnItemClick?) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongHolder {
        return SongHolder(
            ItemAlbumBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ),
            listener

        )
    }

    override fun onBindViewHolder(holder: SongHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<Album>?) {
        super.submitList(list?.let { ArrayList(it) })
    }


}

class SongHolder(
    var binding: ItemAlbumBinding
    , listener: OnItemClick?
) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.itemAlbum.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
    }

    fun bindData(data: Album) {
        Glide.with(itemView.context).load(data.image).circleCrop().into(binding.ivImgAlbum)
        binding.tvAlbumName.text = data.name
    }

}
class SongDiffUtil : DiffUtil.ItemCallback<Album>() {
    override fun areItemsTheSame(oldItem: Album, newItem: Album): Boolean {
        return oldItem.name == newItem.name && oldItem.id == newItem.id && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(oldItem: Album, newItem: Album): Boolean {
        return oldItem == newItem
    }

}
