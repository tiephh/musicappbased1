package com.example.baseproject.ui.playMusic

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.os.IBinder
import android.view.animation.LinearInterpolator
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentPlayMusicBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.SeekBar
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.Navigation
import com.example.baseproject.service.MediaPlayerService
import com.example.core.model.MediaPlayerSong
import com.example.core.utils.Constants
import kotlin.collections.ArrayList
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.view.Window
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.baseproject.databinding.LayoutDialogTimeBinding
import com.example.core.model.Time
import com.example.core.utils.listenner.OnItemClick


@AndroidEntryPoint
class FragmentPlayMusic :
    BaseFragment<FragmentPlayMusicBinding, PlayMusicViewModel>(R.layout.fragment_play_music) {

    @Inject
    lateinit var appNavigation: AppNavigation

    private val intent: Intent? by lazy {
        Intent(activity, MediaPlayerService::class.java)
    }
    private val viewModel: PlayMusicViewModel by viewModels()

    var duration = 0
    private var isPlaying = false
    var message: MediaPlayerSong? = null
    private var isConnect = false
    private var isStop = false
    private var isFav = false

    private var service: MediaPlayerService? = null

    override fun getVM(): PlayMusicViewModel = viewModel

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, mBinder: IBinder) {
            val binder = mBinder as MediaPlayerService.MyBinder
            service = binder.getMediaPlayerService()
            isConnect = true
            serviceInitSuccess()
            viewModel.dataSetup.value?.let { playMusic(it, viewModel.position.value!!) }
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            isConnect = false
        }
    }

    private fun serviceInitSuccess() {
        viewModel.checkFav(service?.nameSong)
        if (service?.isRepeat == true) {
            binding.btnRepeat.setImageResource(R.drawable.ic_baseline_repeat_true_24)
        } else {
            binding.btnRepeat.setImageResource(R.drawable.ic_baseline_repeat_24)
        }
        if (service?.isRandom == true) {
            binding.btnRandom.setImageResource(R.drawable.ic_close_white)
        } else {
            binding.btnRandom.setImageResource(R.drawable.ic_random_mix_icon)
        }
        if (service?.isPlaying == true) {
            binding.btnPlay.setImageResource(R.drawable.ic_baseline_pause_24)
            animation(true)
        } else {
            binding.btnPlay.setImageResource(R.drawable.ic_baseline_play_arrow_24)
            animation(false)
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        activity?.bindService(intent, connection, Context.BIND_AUTO_CREATE)
        init()
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
            mMessageReceiver,
            IntentFilter(Constants.Bundle.SEND_DATA_TO_FRAGMENT)
        )
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
            mReceiver,
            IntentFilter(Constants.Bundle.SEND_DURATION_TO_FRAGMENT)
        )
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
            receiverStop,
            IntentFilter(Constants.Bundle.STOP_SERVICE_FROM_NOTIFY)
        )

    }

    override fun setOnClick() {
        super.setOnClick()
        binding.btnBackHome.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
        binding.addFavourite.setOnClickListener {
            if (isFav) {
                viewModel.isAdd = false
                viewModel.removeFav(service?.nameSong.toString())
            } else {
                viewModel.isAdd = true
                service?.songCurrent?.let { it1 -> viewModel.addFav(it1) }
            }
        }
        binding.btnPlay.setOnClickListener {
            if (!isPlaying) {
                if (isStop) {
                    activity?.bindService(intent, connection, Context.BIND_AUTO_CREATE)
                    isPlaying = true
                    isStop = false
                    playMusic(viewModel.saveState.value!!, viewModel.savePos.value!!)
                } else {
                    if (service?.isPlaying == false) {
                        service?.resumeMusic()
                        isPlaying = true
                        service?.seekTo(binding.sbDuration.progress)
                    }
                    binding.btnPlay.setImageResource(R.drawable.ic_baseline_pause_24)
                    animation(true)
                }
            } else {
                service?.pauseMusic()
                isPlaying = false
                binding.btnPlay.setImageResource(R.drawable.ic_baseline_play_arrow_24)
                animation(false)
            }
        }
        binding.btnNext.setOnClickListener {
            if (isStop) {
                activity?.bindService(intent, connection, Context.BIND_AUTO_CREATE)
                isPlaying = true
                isStop = false
                playMusic(viewModel.saveState.value!!, viewModel.savePos.value!! + 1)
            } else {
                binding.sbDuration.progress = 0
                service?.nextMusic()
                service?.seekTo(0)
                binding.btnPlay.setImageResource(R.drawable.ic_baseline_pause_24)
            }
        }

        binding.btnRewind.setOnClickListener {
            binding.sbDuration.progress = 0
            service?.previousMusic()
            service?.seekTo(0)
            binding.btnPlay.setImageResource(R.drawable.ic_baseline_pause_24)
        }

        binding.sbDuration.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                service?.seekTo(seekBar?.progress)
            }

        })
        binding.btnRepeat.setOnClickListener {
            service?.repeatMusic()
            if (service?.isRepeat == true) {
                binding.btnRepeat.setImageResource(R.drawable.ic_baseline_repeat_true_24)
            } else {
                binding.btnRepeat.setImageResource(R.drawable.ic_baseline_repeat_24)
            }
            if (service?.isRandom == true) {
                service?.randomMusic()
                binding.btnRandom.setImageResource(R.drawable.ic_random_mix_icon)
            }
        }
        binding.btnRandom.setOnClickListener {
            service?.randomMusic()
            if (service?.isRandom == true) {
                binding.btnRandom.setImageResource(R.drawable.ic_close_white)
            } else {
                binding.btnRandom.setImageResource(R.drawable.ic_random_mix_icon)
            }
            if (service?.isRepeat == true) {
                service?.repeatMusic()
                binding.btnRepeat.setImageResource(R.drawable.ic_baseline_repeat_24)
            }
        }
        binding.btnClock.setOnClickListener {
            if (service?.isAlarm == true) {
                service!!.cancelAlarm()
                binding.btnClock.setImageResource(R.drawable.ic_baseline_hourglass_bottom_24)
            } else {
                dateTimeDialog()
            }
        }
    }

    private fun dateTimeDialog() {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val bindingDialog: LayoutDialogTimeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(
                context
            ), R.layout.layout_dialog_time, null, false
        )
        dialog.setContentView(bindingDialog.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val time: ArrayList<Time> = ArrayList()
        time.add(Time(context?.getString(R.string.eight_hour), 28800))
        time.add(Time(context?.getString(R.string.four_houe), 1440))
        time.add(Time(context?.getString(R.string.one_hour), 3600))
        time.add(Time(context?.getString(R.string.thirty_minute), 1800))
        time.add(Time(context?.getString(R.string.fitteen), 900))
        val timeAdapter = TimeAdapter()
        bindingDialog.rvTimePicker.adapter = timeAdapter
        timeAdapter.submitList(time)
        timeAdapter.setOnItemClickListener(object : OnItemClick {
            @SuppressLint("ResourceAsColor")
            override fun onItemClick(position: Int) {
                service?.alarmService(time[position])
                binding.btnClock.setImageResource(R.drawable.ic_close_white)
                binding.tvTimeAlarm.visibility = View.VISIBLE
                binding.tvTimeAlarm.text = time[position].name
                dialog.dismiss()
            }

        })
        bindingDialog.btnClose.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        @SuppressLint("SetTextI18n")
        override fun onReceive(context: Context?, intent: Intent) {
            message = intent.getSerializableExtra(Constants.Bundle.SONG) as MediaPlayerSong
            viewModel.checkFav(message!!.name)
            binding.tvTitleSong.text = message!!.name
            binding.tvSinger.text = message!!.artist
            binding.sbDuration.max = message!!.duration!!
            Glide.with(requireContext()).load(message!!.image).circleCrop()
                .into(binding.ivSongAnimation)
            binding.sbDuration.progress = 0
            binding.tvTimeStart.text = "00:00"
            val minutes = message!!.duration!! / 1000 / 60
            val seconds = message!!.duration!! / 1000 % 60
            if (seconds < 10) {
                binding.tvTimeEnd.text = "$minutes:0$seconds"
            } else {
                binding.tvTimeEnd.text = "$minutes:$seconds"
            }
        }
    }
    private val receiverStop: BroadcastReceiver = object : BroadcastReceiver() {
        @SuppressLint("SetTextI18n")
        override fun onReceive(context: Context?, intent: Intent) {
            if (intent.getBooleanExtra(Constants.Bundle.STOP, false)) {
                binding.btnPlay.setImageResource(R.drawable.ic_baseline_play_arrow_24)
                binding.sbDuration.progress = 0
                binding.tvTimeStart.text = "00:00"
                binding.tvTimeAlarm.visibility = View.GONE
                binding.btnClock.setImageResource(R.drawable.ic_baseline_hourglass_bottom_24)
                animation(false)
                activity?.unbindService(connection)
                isPlaying = false
                isStop = true
            }
            viewModel.saveStateStop(
                intent.getSerializableExtra(Constants.Bundle.SONG)
                        as ArrayList<MediaPlayerSong>,
                intent.getIntExtra(Constants.Bundle.POSITION, 0)
            )
        }
    }
    private val mReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        @SuppressLint("SetTextI18n")
        override fun onReceive(context: Context?, intent: Intent) {
            isConnect = true
            val process = intent.getIntExtra(Constants.Bundle.DURATION, 0)
            binding.sbDuration.progress = process
            val minutes = process / 1000 / 60
            val seconds = process / 1000 % 60
            if (seconds < 10) {
                binding.tvTimeStart.text = "$minutes:0$seconds"
            } else {
                binding.tvTimeStart.text = "$minutes:$seconds"
            }
            if (service?.isPlaying == true) {
                binding.btnPlay.setImageResource(R.drawable.ic_baseline_pause_24)
            } else {
                binding.btnPlay.setImageResource(R.drawable.ic_baseline_play_arrow_24)
                animation(false)
            }

        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(mMessageReceiver)
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(mReceiver)
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(receiverStop)
        if (!isStop) {
            activity?.unbindService(connection)
        }
        isConnect = false
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        viewModel.dataSetup.observe(viewLifecycleOwner) {
            duration = it[viewModel.position.value!!].duration!!
            Glide.with(requireContext()).load(it[viewModel.position.value!!].image)
                .circleCrop()
                .into(binding.ivSongAnimation)
            binding.tvSinger.text = it[viewModel.position.value!!].artist
            binding.tvTitleSong.text = it[viewModel.position.value!!].name
            message = it[viewModel.position.value!!]
            binding.tvTitleSong.text = message!!.name
            binding.tvSinger.text = message!!.artist
        }

        viewModel.saveState.observe(viewLifecycleOwner) {
            duration = it[viewModel.savePos.value!!].duration!!
            Glide.with(requireContext()).load(it[viewModel.savePos.value!!].image)
                .circleCrop()
                .into(binding.ivSongAnimation)
            binding.tvSinger.text = it[viewModel.savePos.value!!].artist
            binding.tvTitleSong.text = it[viewModel.savePos.value!!].name
            message = it[viewModel.savePos.value!!]
            binding.tvTitleSong.text = message!!.name
            binding.tvSinger.text = message!!.artist
        }

        viewModel.dataPlaying.observe(viewLifecycleOwner) {
            binding.tvSinger.text = it?.artist
            binding.tvTitleSong.text = it?.name
            binding.sbDuration.max = it?.duration!!
            val minutes = it.duration!! / 1000 / 60
            val seconds = it.duration!! / 1000 % 60
            if (seconds < 10) {
                binding.tvTimeEnd.text = "$minutes:0$seconds"
            } else {
                binding.tvTimeEnd.text = "$minutes:$seconds"
            }
            Glide.with(requireContext()).load(it.image)
                .circleCrop()
                .into(binding.ivSongAnimation)
        }
        viewModel.isFav.observe(viewLifecycleOwner) {
            isFav = it
            if (it) {
                binding.addFavourite.setImageResource(R.drawable.ic_heart_icon_true)
            } else {
                binding.addFavourite.setImageResource(R.drawable.ic_heart_like_false)
            }
        }
        viewModel.exception.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
        viewModel.success.observe(viewLifecycleOwner) {
            if (it) {
                binding.addFavourite.setImageResource(R.drawable.ic_heart_icon_true)
            } else {
                binding.addFavourite.setImageResource(R.drawable.ic_heart_like_false)
            }
        }
    }
    private fun animation(state: Boolean){
        if (state){
            val rotate = RotateAnimation(
                0F, 360F,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
            )
            rotate.duration = 10000
            rotate.repeatCount = Animation.INFINITE
            rotate.interpolator = LinearInterpolator()
            binding.ivSongAnimation.startAnimation(rotate)
        }else {
            val rotate = RotateAnimation(
                0F, 0F,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
            )
            rotate.duration = 10000
            rotate.repeatCount = Animation.INFINITE
            rotate.interpolator = LinearInterpolator()
            binding.ivSongAnimation.startAnimation(rotate)
        }
    }

    private fun playMusic(song: ArrayList<MediaPlayerSong>, pos: Int) {
        animation(true)
        isPlaying = true
        val bundle = Bundle()
        bundle.putSerializable(Constants.MediaPlayer.PUT_DATA_TO_SERVICE, song)
        bundle.putInt(Constants.MediaPlayer.PUT_POSITION, pos)
        intent?.putExtras(bundle)
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val builder = NetworkRequest.Builder()
        builder.addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)

        val networkRequest = builder.build()
        connectivityManager.registerNetworkCallback(networkRequest,
            object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    activity?.startService(intent)
                    activity?.bindService(intent, connection, Context.BIND_AUTO_CREATE)
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    Toast.makeText(
                        requireContext(),
                        requireContext().getString(R.string.connect),
                        Toast.LENGTH_LONG
                    ).show()
                }
            })
        service?.first = true
        activity?.startService(intent)
    }
}