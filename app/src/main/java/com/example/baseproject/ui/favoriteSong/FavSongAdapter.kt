package com.example.baseproject.ui.favoriteSong

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemAlbumDetailBinding
import com.example.core.model.MediaPlayerSong

class FavSongAdapter : ListAdapter<MediaPlayerSong, FavHolder>(FavDiffUtil()){

    interface IOnClickListener{
        fun onItemClick(position: Int)
        fun deleteItem(position: Int)
    }

    var listener: IOnClickListener? = null

    fun setOnItemClickListener(listener: IOnClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavHolder {
        return FavHolder(
            ItemAlbumDetailBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ),listener
        )
    }

    override fun onBindViewHolder(holder: FavHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<MediaPlayerSong>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

}

class FavHolder(
    var binding: ItemAlbumDetailBinding
    , listener: FavSongAdapter.IOnClickListener?
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.ctItemAlbumDetail.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }

        val popupMenu = PopupMenu(itemView.context, binding.btnMenuPopup)
        popupMenu.menuInflater.inflate(R.menu.popup_menu_full, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener,
            PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                if (item.itemId == R.id.menu_delete){
                    listener?.deleteItem(adapterPosition)
                }
                return true
            }
        })

        binding.btnMenuPopup.setOnClickListener {
            popupMenu.show()
        }
    }

    fun bindData(data: MediaPlayerSong) {
        Glide.with(itemView.context).load(data.image).circleCrop().into(binding.ivAlbumDetail)
        binding.tvAlbumNameDetail.text = data.name
        binding.tvAlbumNameSingerDetail.text = data.artist

    }

}
class FavDiffUtil : DiffUtil.ItemCallback<MediaPlayerSong>() {
    override fun areItemsTheSame(oldItem: MediaPlayerSong, newItem: MediaPlayerSong): Boolean {
        return oldItem.name == newItem.name && oldItem.image == newItem.image && oldItem.source == newItem.source
    }

    override fun areContentsTheSame(oldItem: MediaPlayerSong, newItem: MediaPlayerSong): Boolean {
        return oldItem == newItem
    }

}