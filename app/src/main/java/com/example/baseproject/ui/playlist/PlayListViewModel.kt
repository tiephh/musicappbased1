package com.example.baseproject.ui.playlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.core.base.BaseViewModel
import com.example.core.database.PlaylistHelper
import com.example.core.model.MediaPlayerSong
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class PlayListViewModel @Inject constructor(private val playlist: PlaylistHelper,
 private val repository: PlaylistRepository) :BaseViewModel() {

    val dataPlaylist = MutableLiveData<ArrayList<MediaPlayerSong>>()
    private val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }
    init {
        readPlaylist()
    }

    fun readPlaylist() {
        viewModelScope.launch(Dispatchers.IO + handler) {
            val artist = repository.getDataSinger()
            val array = ArrayList<MediaPlayerSong>()
            var artistName = ""
            for (i in playlist.readAllSong().indices){
                for (j in artist.indices){
                    if (artist[j].id?.equals(playlist.readAllSong()[i].artistId) == true){
                        artistName = artist[j].name.toString()
                    }
                }
                array.add(MediaPlayerSong(playlist.readAllSong()[i].id
                    ,playlist.readAllSong()[i].title,
                    playlist.readAllSong()[i].image,
                    artistName
                ,playlist.readAllSong()[i].source
                ,playlist.readAllSong()[i].duration))
            }
            dataPlaylist.postValue(array)
        }
    }
}