package com.example.baseproject.ui.genrees

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemAlbumDetailBinding
import com.example.core.model.AlbumDetail
import com.example.core.utils.listenner.IOnClickListener

class GenresAdapter : ListAdapter<AlbumDetail, GenresHolder>(GenresDiffUtil()){

    var listener: IOnClickListener? = null

    fun setOnItemClickListener(listener: IOnClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenresHolder {
        return GenresHolder(
            ItemAlbumDetailBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ),listener
        )
    }

    override fun onBindViewHolder(holder: GenresHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<AlbumDetail>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

}

class GenresHolder(
    var binding: ItemAlbumDetailBinding
    , listener: IOnClickListener?
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.ctItemAlbumDetail.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }

        val popupMenu = PopupMenu(itemView.context, binding.btnMenuPopup)
        popupMenu.menuInflater.inflate(R.menu.menu_popup, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener,
            PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                if (item.itemId == R.id.menu_insert){
                    listener?.insertItem(adapterPosition)
                }else if (item.itemId == R.id.menu_fav){
                    listener?.insertFav(adapterPosition)
                }
                return true
            }
        })

        binding.btnMenuPopup.setOnClickListener {
            popupMenu.show()
        }
    }

    fun bindData(data: AlbumDetail) {
        Glide.with(itemView.context).load(data.image).circleCrop().into(binding.ivAlbumDetail)
        binding.tvAlbumNameDetail.text = data.title
        binding.tvAlbumNameSingerDetail.text = data.singer

    }

}
class GenresDiffUtil : DiffUtil.ItemCallback<AlbumDetail>() {
    override fun areItemsTheSame(oldItem: AlbumDetail, newItem: AlbumDetail): Boolean {
        return oldItem.title == newItem.title && oldItem.image == newItem.image && oldItem.singer == newItem.singer
    }

    override fun areContentsTheSame(oldItem: AlbumDetail, newItem: AlbumDetail): Boolean {
        return oldItem == newItem
    }

}