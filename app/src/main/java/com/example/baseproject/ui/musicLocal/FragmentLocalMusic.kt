package com.example.baseproject.ui.musicLocal

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentMusicLocalBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.utils.Constants
import com.example.core.utils.listenner.OnItemClick
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class FragmentLocalMusic : BaseFragment<FragmentMusicLocalBinding, LocalViewModel>(R.layout.fragment_music_local) {

    private val viewModel: LocalViewModel by viewModels()
    override fun getVM(): LocalViewModel  = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    private val adapter: MusicLocalAdapter by lazy {
        MusicLocalAdapter()
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        setupAdapter()
        setupObserver()

    }

    override fun setOnClick() {
        super.setOnClick()
        binding.btnBackHome.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
        adapter.setOnItemClickListener(object : OnItemClick{
            override fun onItemClick(position: Int) {
                val bundle = Bundle()
                bundle.putSerializable(Constants.MediaPlayer.PUT_DATA_TO_SERVICE, viewModel.localPlay.value)
                bundle.putInt(Constants.MediaPlayer.PUT_POSITION, position)
                appNavigation.openPlay(bundle)
            }
        })
    }

    private fun setupAdapter() {
        binding.rvLocal.adapter = adapter
    }

    private fun setupObserver() {
        viewModel.dataLocal.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
    }
}