package com.example.baseproject.ui.tabHomePage


import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.example.baseproject.R
import com.example.baseproject.Ultils.Contants
import com.example.baseproject.databinding.FragmentHomePageBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.FavouriteHelper
import com.example.core.model.FavouriteArtist
import com.example.core.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomePageFragment :
    BaseFragment<FragmentHomePageBinding, HomePageViewModel>(R.layout.fragment_home_page){

    private val viewModel: HomePageViewModel by viewModels()

    override fun getVM(): HomePageViewModel = viewModel

    interface RemoveHandler{
        fun remove()
    }

    val listener: RemoveHandler? = null

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var favourite: FavouriteHelper

    private val adapter: AdapterHomePage by lazy {
        AdapterHomePage()
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupObserver()
        setupRecycle()
    }

    override fun setOnClick() {
        super.setOnClick()
        adapter.run {
            setOnItemClickListener(object : AdapterHomePage.OnClickHome {
                override fun click(position: Int, dataOf: String) {
                    when (dataOf) {
                        Constants.Object.ARTIST -> {
                            val bundle = Bundle()
                            bundle.putString(Contants.ARTIST_ID, viewModel.dataArtist.value?.get(position)?.id.toString())
                            bundle.putString(Contants.ARTIST_IMAGE, viewModel.dataArtist.value?.get(position)?.image)
                            bundle.putString(Contants.ARTIST_NAME, viewModel.dataArtist.value?.get(position)?.name)
                            appNavigation.openHomePageArtist(bundle)
                        }
                        Constants.Object.ALBUM -> {
                            val bundle = Bundle()
                            bundle.putString(Contants.ALBUM_ID, viewModel.dataSong.value?.get(position)?.id.toString())
                            bundle.putString(Contants.ALBUM_IMAGE, viewModel.dataSong.value?.get(position)?.image)
                            bundle.putString(Contants.AlBUM_TITLE, viewModel.dataSong.value?.get(position)?.name)
                            bundle.putString(Contants.ALBUM_TOTAL_SONG, viewModel.dataSong.value?.get(position)?.totalSong)
                            appNavigation.openHomePageToAlbum(bundle)
                        }
                        Constants.Object.GENREES -> {
                            val bundle = Bundle()
                            bundle.putString(Contants.ALBUM_ID, viewModel.dataGenrees.value?.get(position)?.id.toString())
                            bundle.putString(Contants.ALBUM_IMAGE, viewModel.dataGenrees.value?.get(position)?.image)
                            bundle.putString(Contants.AlBUM_TITLE, viewModel.dataGenrees.value?.get(position)?.name)
                            appNavigation.openHomeToGenres(bundle)
                        }
                        Constants.Object.SUGGEST -> {
                            val bundle = Bundle()
                            bundle.putSerializable(Constants.MediaPlayer.PUT_DATA_TO_SERVICE,
                                viewModel.dataPlay.value
                            )
                            bundle.putInt(Constants.MediaPlayer.PUT_POSITION, position)
                            appNavigation.openHomeToPlay(bundle)
                        }
                    }
                }

            })
            setOnClickListener(object : AdapterHomePage.CommunicationDataBase{
                override fun clickUnCare(data: FavouriteArtist) {
                    favourite.deleteFavourite(data.id.toString())
                    viewModel.getListSong()
                    adapter.submitList(viewModel.dataHome.value)
                }

                override fun clickCare(data: FavouriteArtist) {
                    favourite.insertFavourite(FavouriteArtist(data.name, data.image, null, 1, data.idFav))
                    viewModel.getListSong()
                    adapter.submitList(viewModel.dataHome.value)
                }

            })
        }
    }

    private fun setupRecycle() {
        binding.rvHomePage.adapter = adapter
    }


    private fun setupObserver() {
        viewModel.dataSong.observe(viewLifecycleOwner) {
        }
        viewModel.dataArtist.observe(viewLifecycleOwner) {

        }
        viewModel.dataSuggest.observe(viewLifecycleOwner) {

        }
        viewModel.dataGenrees.observe(viewLifecycleOwner) {
        }

        viewModel.dataHome.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
        viewModel.loading.observe(viewLifecycleOwner) {
            if (it) {
                binding.pbLoading.visibility = View.VISIBLE
            } else {
                binding.pbLoading.visibility = View.GONE
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        listener?.remove()
    }

}