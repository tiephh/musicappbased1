package com.example.baseproject.ui.tabHomePage

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.baseproject.databinding.ItemSlideImageBinding
import java.text.ParseException

class ViewPagerAdapter(val context: Context, private var dataValue: ArrayList<String>) :
    RecyclerView.Adapter<ViewPagerAdapter.ViewPagerViewHolder>() {


    @SuppressLint("NotifyDataSetChanged")
    fun setData(array: ArrayList<String>){
        dataValue = array
        notifyDataSetChanged()
    }

    inner class ViewPagerViewHolder(val binding: ItemSlideImageBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(image: String){
            Glide.with(context).load(image).into(binding.ivItemSlide)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerViewHolder {
        return ViewPagerViewHolder(
            ItemSlideImageBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    }
    override fun onBindViewHolder(holder: ViewPagerViewHolder, position: Int) {
        val image: String = dataValue[position]
        try {
            holder.bind(image)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

    }

    override fun getItemCount(): Int = dataValue.size


}