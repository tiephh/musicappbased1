package com.example.baseproject.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.baseproject.R
import com.example.baseproject.databinding.LoginFragmentBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class LoginFragment: BaseFragment<LoginFragmentBinding, LoginViewModel>(R.layout.login_fragment) {

    private val viewModel: LoginViewModel by viewModels()

    @Inject
    lateinit var appNavigation: AppNavigation

    override fun getVM(): LoginViewModel = viewModel

    override fun setOnClick() {
        super.setOnClick()
        binding.btnLogin.setOnClickListener {
            if (binding.etEmail.text.length >= 8 && binding.etPass.text.length >= 8){
                viewModel.login(binding.etEmail.text.toString(), binding.etPass.text.toString())
            }else{
                Toast.makeText(requireContext(), requireContext().getString(R.string.dont_input), Toast.LENGTH_LONG).show()
            }
        }
        binding.tvRegister.setOnClickListener {
            appNavigation.openToRegister()
        }
        binding.btnBackHome.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupObserver()
        binding.etEmail.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (binding.etEmail.text.length >= 8 && binding.etPass.text.length >= 8){
                    binding.btnLogin.setBackgroundResource(R.drawable.button_custom_blue)
                }else{
                    binding.btnLogin.setBackgroundResource(R.drawable.button_custom)
                }
            }

        })
        binding.etPass.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (binding.etEmail.text.length >= 8 && binding.etPass.text.length >= 8){
                    binding.btnLogin.setBackgroundResource(R.drawable.button_custom_blue)
                }else{
                    binding.btnLogin.setBackgroundResource(R.drawable.button_custom)
                }
            }

        })
    }

    private fun setupObserver() {
        viewModel.responseLogin.observe(viewLifecycleOwner) {
            if (it) {
                appNavigation.openLoginToHome()
                Toast.makeText(
                    requireContext(),
                    requireContext().getString(R.string.login_success),
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    requireContext(),
                    requireContext().getString(R.string.login_fail),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        viewModel.exception.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

}