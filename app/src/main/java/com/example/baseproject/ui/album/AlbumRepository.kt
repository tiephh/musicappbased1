package com.example.baseproject.ui.album

import com.example.core.model.Song
import com.example.core.model.Artist
import com.example.core.network.RetrofitService
import javax.inject.Inject

class AlbumRepository @Inject constructor(private val retrofit: RetrofitService) {

    suspend fun getDataAlbumByKey(): ArrayList<Song> = retrofit.getMusic()
    suspend fun getDataSinger(): ArrayList<Artist> = retrofit.getArtist()
}