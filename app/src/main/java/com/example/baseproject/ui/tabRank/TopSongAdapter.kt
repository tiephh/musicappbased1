package com.example.baseproject.ui.tabRank

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemTopSongBinding
import com.example.core.model.AlbumDetail

class TopSongAdapter constructor(
    context: Context,
) : ListAdapter<AlbumDetail, TopHolder>(TopDiffUtil()){
    private var mContext: Context? = null

    interface OnItemClick {
        fun onItemClick(position: Int)
        fun onItemMenu(position: Int)
    }

    var listener: OnItemClick? = null

    fun setOnItemClickListener(listener: OnItemClick?) {
        this.listener = listener
    }

    init {
        mContext = context
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopHolder {
        return TopHolder(
            ItemTopSongBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ), listener
            )
    }

    override fun onBindViewHolder(holder: TopHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<AlbumDetail>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).title.hashCode().toLong()
    }

}

class TopHolder(
    var binding: ItemTopSongBinding,
    listener: TopSongAdapter.OnItemClick?
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.ctTopSong.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
        val popupMenu = PopupMenu(itemView.context, binding.btnMenuPopup)
        popupMenu.menuInflater.inflate(R.menu.menu_popup, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener,
            PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                if (item.itemId == R.id.menu_insert){
                    listener?.onItemMenu(adapterPosition)
                }
                return true
            }
        })

        binding.btnMenuPopup.setOnClickListener {
            popupMenu.show()
        }
    }
    @SuppressLint("SetTextI18n")
    fun bindData(data: AlbumDetail) {
        Glide.with(itemView.context).load(data.image).into(binding.ivTopSong)
        binding.tvTopName.text = data.title
        binding.tvSingerTop.text = data.singer
        binding.tvNumber.text = (adapterPosition+1).toString()
        val paint = binding.tvNumber.paint
        val width = paint.measureText(binding.tvNumber.text.toString())
        val textShader: Shader = LinearGradient(0f, 0f, width, binding.tvNumber.textSize, intArrayOf(
            Color.parseColor("#ff1a1a"),
            Color.parseColor("#ff80bf"),
            Color.parseColor("#ffff1a"),
            Color.parseColor("#2eb82e"),
            Color.parseColor("#ff1a1a")
        ), null, Shader.TileMode.REPEAT)

        binding.tvNumber.paint.shader = textShader
    }

}
class TopDiffUtil : DiffUtil.ItemCallback<AlbumDetail>() {
    override fun areItemsTheSame(oldItem: AlbumDetail, newItem: AlbumDetail): Boolean {
        return oldItem.title == newItem.title && oldItem.singer == newItem.singer && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(oldItem: AlbumDetail, newItem: AlbumDetail): Boolean {
        return oldItem == newItem
    }

}
