package com.example.baseproject.ui.playlist

import com.example.core.model.Artist
import com.example.core.network.RetrofitService
import javax.inject.Inject

class PlaylistRepository @Inject constructor(private val retrofit: RetrofitService) {
    suspend fun getDataSinger(): ArrayList<Artist> = retrofit.getArtist()
}