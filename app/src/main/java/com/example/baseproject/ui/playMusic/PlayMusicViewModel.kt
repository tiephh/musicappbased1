package com.example.baseproject.ui.playMusic

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.baseproject.R
import com.example.core.base.BaseViewModel
import com.example.core.model.MediaPlayerSong
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

@HiltViewModel
class PlayMusicViewModel @Inject constructor(private val savedStateHandle: SavedStateHandle, private val appPreferences: AppPreferences,
@ApplicationContext private val context: Context) : BaseViewModel() {

    val dataSetup = MutableLiveData<ArrayList<MediaPlayerSong>>()
    val position = MutableLiveData<Int>()
    val dataPlaying = MutableLiveData<MediaPlayerSong?>()
    val isFav = MutableLiveData<Boolean>()
    val exception = MutableLiveData<String?>()
    val success = MutableLiveData<Boolean>()
    val saveState = MutableLiveData<ArrayList<MediaPlayerSong>>()
    val savePos = MutableLiveData<Int>()
    var isAdd = true

    init {
        setupView()
    }

    private fun setupView(){
        val dataIsPlaying: MediaPlayerSong? = savedStateHandle.get(Constants.Bundle.DATA_PLAYING)
        if (dataIsPlaying != null){
            dataPlaying.postValue(dataIsPlaying)
        }else{
            val dataListSong: ArrayList<MediaPlayerSong>? = savedStateHandle.get(Constants.MediaPlayer.PUT_DATA_TO_SERVICE)
            val dataPos: Int? = savedStateHandle.get(Constants.MediaPlayer.PUT_POSITION)
            position.postValue(dataPos!!)
            dataSetup.postValue(dataListSong!!)
        }
    }

    fun checkFav(song : String?){
        appPreferences.getDatabaseReference(Constants.Firebase.FAVOURITE)
            .child(appPreferences.getFirebaseUid())
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    var isCheck = false
                    for (dataSnapshot in snapshot.children ){
                        dataSnapshot.getValue(MediaPlayerSong::class.java)?.let {
                            if (it.name.equals(song)){
                                isCheck = true
                            }
                        }
                    }
                    isAdd = if (isCheck){
                        isFav.postValue(true)
                        true
                    }else{
                        isFav.postValue((false))
                        false
                    }
                }
                override fun onCancelled(error: DatabaseError) {
                }
            })
    }
    fun removeFav(name: String){
        appPreferences.getDatabaseReference(Constants.Firebase.FAVOURITE)
            .child(appPreferences.getFirebaseUid())
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (dataSnapshot in snapshot.children ){
                        dataSnapshot.getValue(MediaPlayerSong::class.java)?.let {
                            if (!isAdd){
                                if (it.name.equals(name)){
                                    dataSnapshot.ref.removeValue()
                                    isAdd = true
                                    success.postValue(false)
                                }
                            }
                        }
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                }

            })
    }

    fun addFav(song: MediaPlayerSong){
        if (FirebaseAuth.getInstance().currentUser != null) {
            appPreferences.setSongFirebase(song)
            success.postValue(true)
        }else{
            exception.postValue(context.getString(R.string.none_acc))
            success.postValue(false)
        }
    }
    fun saveStateStop(song: ArrayList<MediaPlayerSong>, pos: Int){
        savePos.postValue(pos)
        saveState.postValue(song)
    }
}