package com.example.baseproject.ui.artist

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentArtistBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.utils.Constants
import com.example.core.utils.isNetworkConnected
import com.example.core.utils.listenner.IOnClickListener
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FragmentArtist :
    BaseFragment<FragmentArtistBinding, ArtistViewModel>(R.layout.fragment_artist) {

    private val viewModel: ArtistViewModel by viewModels()
    override fun getVM(): ArtistViewModel = viewModel

    private val adapterArtist: ArtistAdapterDetail by lazy {
        ArtistAdapterDetail()
    }

    @Inject
    lateinit var appNavigation: AppNavigation

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObserver()
        setupAdapter()
    }

    override fun setOnClick() {
        super.setOnClick()
        binding.btnBackHome.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
        adapterArtist.setOnItemClickListener(object : IOnClickListener {
            override fun onItemClick(position: Int) {
                if (activity?.isNetworkConnected() == true){
                    val bundle = Bundle()
                    bundle.putSerializable(Constants.MediaPlayer.PUT_DATA_TO_SERVICE, viewModel.dataSetup.value)
                    bundle.putInt(Constants.MediaPlayer.PUT_POSITION, position)
                    appNavigation.openPlay(bundle)
                }else{
                    Toast.makeText(requireContext(), context?.getString(R.string.connect), Toast.LENGTH_LONG).show()
                }
            }

            override fun insertItem(position: Int) {
                val dialogClickListener =
                    DialogInterface.OnClickListener { _, which ->
                        when (which) {
                            DialogInterface.BUTTON_POSITIVE -> viewModel.insertData(viewModel.dataSong.value?.get(position))
                            DialogInterface.BUTTON_NEGATIVE -> {}
                        }
                    }

                val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                builder.setMessage(requireContext().getText(R.string.want_insert)).setPositiveButton(requireContext().getString(R.string.yes), dialogClickListener)
                    .setNegativeButton(requireContext().getString(R.string.no), dialogClickListener).show()
            }

            override fun insertFav(position: Int) {
                viewModel.insertFav(viewModel.dataSetup.value?.get(position)!!)
            }

        })
    }

    private fun setupAdapter() {
        binding.rvArtistDetail.adapter = adapterArtist

    }

    private fun setupObserver() {
        viewModel.artistImage.observe(viewLifecycleOwner) {
            Glide.with(requireContext()).load(it).transform(CenterInside(), RoundedCorners(24))
                .into(binding.ivArtistImg)
        }
        viewModel.artistName.observe(viewLifecycleOwner) {
            binding.tvArtistName.text = it
        }
        viewModel.loading.observe(viewLifecycleOwner) {
            if (it) {
                binding.pbLoading.visibility = View.VISIBLE
            } else {
                binding.pbLoading.visibility = View.GONE
            }
        }
        viewModel.dataSong.observe(viewLifecycleOwner) {
            adapterArtist.submitList(it)
        }
        viewModel.exception.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }
}