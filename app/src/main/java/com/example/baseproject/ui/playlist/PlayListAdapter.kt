package com.example.baseproject.ui.playlist

import android.net.Uri
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemPlayListBinding
import com.example.core.model.MediaPlayerSong

class PlayListAdapter : ListAdapter<MediaPlayerSong, PlayListHolder>(PlayListDiffUtil()){

    interface ClickItem{
        fun clickItem(position: Int)
        fun clickMenu(position: Int)
    }

    private var listener: ClickItem? = null

    fun setOnItemClickListener(listener: ClickItem?) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayListHolder {
        return PlayListHolder(
            ItemPlayListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ),
            listener

        )
    }

    override fun onBindViewHolder(holder: PlayListHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<MediaPlayerSong>?) {
        super.submitList(list?.let { ArrayList(it) })
    }


}

class PlayListHolder(
    var binding: ItemPlayListBinding
    , listener: PlayListAdapter.ClickItem?
) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.ctItemAlbumDetail.setOnClickListener {
            listener?.clickItem(adapterPosition)
        }
        val popupMenu = PopupMenu(itemView.context, binding.btnMenu)
        popupMenu.menuInflater.inflate(R.menu.popup_menu_full, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener,
            PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                if (item.itemId == R.id.menu_delete){
                    listener?.clickMenu(adapterPosition)
                }
                return true
            }
        })
        binding.btnMenu.setOnClickListener {
            popupMenu.show()
        }
    }

    fun bindData(data: MediaPlayerSong) {
        val uri: Uri = Uri.parse(data.image)
        Glide.with(itemView.context).load(uri)
            .transform(CenterInside(), RoundedCorners(24)).into(binding.ivAlbumDetail)
        binding.tvAlbumNameDetail.text = data.name
        binding.tvArtist.text = data.artist
    }

}
class PlayListDiffUtil : DiffUtil.ItemCallback<MediaPlayerSong>() {
    override fun areItemsTheSame(oldItem: MediaPlayerSong, newItem: MediaPlayerSong): Boolean {
        return oldItem.name == newItem.name && oldItem.id == newItem.id && oldItem.image == newItem.image
                && oldItem.source == newItem.source && oldItem.duration == newItem.duration
    }

    override fun areContentsTheSame(oldItem: MediaPlayerSong, newItem: MediaPlayerSong): Boolean {
        return oldItem == newItem
    }

}