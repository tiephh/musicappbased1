package com.example.baseproject.ui.personal

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.databinding.ItemFavouriteBinding
import com.example.core.model.FavouriteArtist

class AdapterFavourite : ListAdapter<FavouriteArtist, FavouriteHolder>(FavouriteDiffUtil()){

    interface OnClickArtist{
        fun itemClick(data: FavouriteArtist)
    }

    private var listener: OnClickArtist? = null

    fun setOnItemClickListener(listener: OnClickArtist?) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteHolder {
        return FavouriteHolder(
            ItemFavouriteBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ), listener)
    }

    override fun onBindViewHolder(holder: FavouriteHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<FavouriteArtist>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

}

class FavouriteHolder(
    var binding: ItemFavouriteBinding
    , val listener: AdapterFavourite.OnClickArtist?,
) : RecyclerView.ViewHolder(binding.root) {

    fun bindData(data: FavouriteArtist) {
        binding.tvName.text = data.name
        Glide.with(itemView.context).load(data.image).transform(CenterInside(), RoundedCorners(24)).into(binding.ivAvaArtist)
        binding.ctLayout.setOnClickListener {
            listener?.itemClick(data)
        }
    }

}
class FavouriteDiffUtil : DiffUtil.ItemCallback<FavouriteArtist>() {
    override fun areItemsTheSame(oldItem: FavouriteArtist, newItem: FavouriteArtist): Boolean {
        return oldItem.name == newItem.name && oldItem.id == newItem.id && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(oldItem: FavouriteArtist, newItem: FavouriteArtist): Boolean {
        return oldItem == newItem
    }

}
