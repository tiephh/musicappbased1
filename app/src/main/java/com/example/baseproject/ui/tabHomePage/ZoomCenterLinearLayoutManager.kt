package com.example.baseproject.ui.tabHomePage

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler
import kotlin.math.abs


internal class ZoomCenterLinearLayoutManager : LinearLayoutManager {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, orientation: Int, reverseLayout: Boolean) : super(
        context,
        orientation,
        reverseLayout
    )

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun checkLayoutParams(lp: RecyclerView.LayoutParams): Boolean {
        lp.width = width / 3
        return true
    }

    override fun onLayoutCompleted(state: RecyclerView.State) {
        super.onLayoutCompleted(state)
        scaleMiddle()
    }

    override fun scrollHorizontallyBy(dx: Int, recycler: Recycler, state: RecyclerView.State): Int {
        scaleMiddle()
        return super.scrollHorizontallyBy(dx, recycler, state)
    }

    private fun scaleMiddle() {
        val midpoint = width / 2f
        for (i in 0 until childCount) {
            val child: View? = getChildAt(i)
            val childMidpoint =
                (getDecoratedRight(child!!) + getDecoratedLeft(child)) / 2f
            val d =
                abs(midpoint - childMidpoint)
            val scale =
                1f - 0.35f * d / midpoint
            child.scaleX = scale
            child.scaleY = scale
        }
    }
}
