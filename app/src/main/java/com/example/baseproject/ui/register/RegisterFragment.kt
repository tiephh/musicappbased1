package com.example.baseproject.ui.register

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.baseproject.R
import com.example.baseproject.databinding.RegisterFragmentBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.pref.AppPreferences
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class RegisterFragment: BaseFragment<RegisterFragmentBinding, RegisterViewModel>(R.layout.register_fragment) {

    private val viewModel: RegisterViewModel by viewModels()
    override fun getVM(): RegisterViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var ref: AppPreferences

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupObserver()
        setupView()
    }

    private fun setupView() {
        binding.etEmail.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (binding.etEmail.text.length >= 8 && binding.etPass.text.length >= 8 && binding.etUserName.text.isNotEmpty()){
                    binding.btnRegister.setBackgroundResource(R.drawable.button_custom_blue)
                }else{
                    binding.btnRegister.setBackgroundColor(R.drawable.button_custom)
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })
        binding.etPass.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (binding.etEmail.text.length >= 8 && binding.etPass.text.length >= 8 && binding.etUserName.text.isNotEmpty()){
                    binding.btnRegister.setBackgroundResource(R.drawable.button_custom_blue)
                }else{
                    binding.btnRegister.setBackgroundColor(R.drawable.button_custom)
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })
        binding.etUserName.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (binding.etEmail.text.length >= 8 && binding.etPass.text.length >= 8 && binding.etUserName.text.isNotEmpty()){
                    binding.btnRegister.setBackgroundResource(R.drawable.button_custom_blue)
                }else{
                    binding.btnRegister.setBackgroundColor(R.drawable.button_custom)
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })
    }

    private fun setupObserver() {
        viewModel.resultData.observe(viewLifecycleOwner) {
            if (it) {
                appNavigation.openRegisterToHome()
            } else {
                Toast.makeText(requireContext(), requireContext().getString(R.string.fail), Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun setOnClick() {
        super.setOnClick()
        binding.btnRegister.setOnClickListener {
            if (binding.etEmail.text.length >= 8 && binding.etPass.text.length >= 8){
                viewModel.register(binding.etEmail.text.toString(), binding.etPass.text.toString(), binding.etUserName.text.toString())
                Toast.makeText(requireContext(), requireContext().getString(R.string.res_success), Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(requireContext(),requireContext().getString(R.string.dont_input) , Toast.LENGTH_LONG).show()
            }
        }
        binding.tvLogin.setOnClickListener {
            appNavigation.registerToLogin()
        }
    }

}