package com.example.baseproject.ui.tabHomePage

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.core.utils.listenner.OnItemClick
import com.example.baseproject.databinding.ItemGenreesBinding
import com.example.core.model.Genrees

class GenresAdapter : ListAdapter<Genrees, GenresHolder>(GenresDiffUtil()){

    var listener: OnItemClick? = null

    fun setOnItemClickListener(listener: OnItemClick?) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenresHolder {
        return GenresHolder(
            ItemGenreesBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            , listener
        )
    }

    override fun onBindViewHolder(holder: GenresHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<Genrees>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

}

class GenresHolder(
    var binding: ItemGenreesBinding
    , listener: OnItemClick?
) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.ivGenrees.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
        binding.tvGenrees.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
    }

    fun bindData(data: Genrees) {
        binding.tvGenrees.text = data.name
        Glide.with(itemView.context).load(data.image).centerCrop().into(binding.ivGenrees)

    }

}
class GenresDiffUtil : DiffUtil.ItemCallback<Genrees>() {
    override fun areItemsTheSame(oldItem: Genrees, newItem: Genrees): Boolean {
        return oldItem.name == newItem.name && oldItem.id == newItem.id && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(oldItem: Genrees, newItem: Genrees): Boolean {
        return oldItem == newItem
    }

}
