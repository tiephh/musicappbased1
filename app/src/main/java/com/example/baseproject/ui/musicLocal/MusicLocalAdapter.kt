package com.example.baseproject.ui.musicLocal

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.databinding.ItemSuggestSongBinding
import com.example.core.model.SongLocal
import com.example.core.utils.listenner.OnItemClick

class MusicLocalAdapter: ListAdapter<SongLocal, SongLocalHolder>(SongLocalDiffUtil()){

    private var listener: OnItemClick? = null

    fun setOnItemClickListener(listener: OnItemClick?) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongLocalHolder {
        return SongLocalHolder(
            ItemSuggestSongBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ),
            listener

        )
    }

    override fun onBindViewHolder(holder: SongLocalHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<SongLocal>?) {
        super.submitList(list?.let { ArrayList(it) })
    }


}

class SongLocalHolder(
    var binding: ItemSuggestSongBinding
    , listener: OnItemClick?
) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.ctItemAlbumDetail.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
    }

    fun bindData(data: SongLocal) {
        val uri: Uri = Uri.parse(data.image)
        Glide.with(itemView.context).load(uri)
            .transform(CenterInside(), RoundedCorners(24)).into(binding.ivAlbumDetail)
        binding.tvAlbumNameDetail.text = data.title
        binding.tvArtist.text = data.artist
    }

}
class SongLocalDiffUtil : DiffUtil.ItemCallback<SongLocal>() {
    override fun areItemsTheSame(oldItem: SongLocal, newItem: SongLocal): Boolean {
        return oldItem.title == newItem.title && oldItem.id == newItem.id && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(oldItem: SongLocal, newItem: SongLocal): Boolean {
        return oldItem == newItem
    }

}
