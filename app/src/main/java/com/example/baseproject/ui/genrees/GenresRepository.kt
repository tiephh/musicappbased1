package com.example.baseproject.ui.genrees

import com.example.core.model.Song
import com.example.core.model.Artist
import com.example.core.network.RetrofitService
import javax.inject.Inject

class GenresRepository @Inject constructor(private val retrofit: RetrofitService){

    suspend fun getDataGenresByKey(): ArrayList<Song> = retrofit.getMusic()
    suspend fun getDataSinger(): ArrayList<Artist> = retrofit.getArtist()
}