package com.example.baseproject.ui.tabRank

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.core.base.BaseViewModel
import com.example.core.model.AlbumDetail
import com.example.core.model.MediaPlayerSong
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class RankViewModel @Inject constructor(private val repository: RankRepository) : BaseViewModel() {
    val dataTop = MutableLiveData<ArrayList<AlbumDetail>>()
    val dataSetup = MutableLiveData<ArrayList<MediaPlayerSong>>()
    val loading = MutableLiveData<Boolean>()

    private val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    init {
        getListDataTop()
    }

    private fun getListDataTop() {
        viewModelScope.launch(Dispatchers.IO + handler) {
            loading.postValue(true)
            try {
                val responseSong = async { repository.getTopSong() }
                val responseArtist = async { repository.getArtist() }
                val array: ArrayList<AlbumDetail> = ArrayList()

                for (i in responseSong.await().indices) {
                    var nameArtist = ""
                    for (j in responseArtist.await().indices) {
                        if (responseSong.await()[i].artistId.toString() == responseArtist.await()[j].id.toString()) {
                            nameArtist = responseArtist.await()[j].name.toString()
                        }
                    }
                    array.add(
                        AlbumDetail(
                            responseSong.await()[i].image,
                            nameArtist,
                            responseSong.await()[i].title,
                            responseSong.await()[i].source,
                            responseSong.await()[i].duration
                        )
                    )
                }
                val listSetup: ArrayList<MediaPlayerSong> = ArrayList()
                for (i in array.indices) {
                    val song = MediaPlayerSong(
                        1, array[i].title,
                        array[i].image,
                        array[i].singer,
                        array[i].song,
                        array[i].duration?.times(1000)
                    )
                    listSetup.add(song)
                }
                dataSetup.postValue(listSetup)
                dataTop.postValue(array)
                loading.postValue(false)
            } catch (e: Exception) {
                dataTop.postValue(ArrayList())
                loading.postValue(true)
            }
        }
    }
}
