package com.example.baseproject.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.core.base.BaseViewModel
import com.example.core.model.Users
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class LoginViewModel @Inject constructor(val savedStateHandle: SavedStateHandle, private val ref: AppPreferences): BaseViewModel() {
    private val auth: FirebaseAuth = Firebase.auth
    val responseLogin = MutableLiveData<Boolean>()
    val exception = MutableLiveData<String>()

    fun login(email:String, pass:String){
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    responseLogin.postValue(true)
                    ref.getDatabaseReference(Constants.Firebase.USER).child(auth.currentUser?.uid.toString())
                        .addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val users: Users? = snapshot.getValue(Users::class.java)
                                if (users != null) {
                                    ref.setDataUsers(users)
                                }else{
                                    val users = Users(null, null, null)
                                    ref.setDataUsers(users)
                                }
                            }
                            override fun onCancelled(error: DatabaseError) {
                                exception.postValue(error.toString())
                            }

                        })
                } else {
                    responseLogin.postValue(false)
                }
            }
    }
}