package com.example.baseproject.ui.favoriteSong

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.baseproject.R
import com.example.core.base.BaseViewModel
import com.example.core.model.MediaPlayerSong
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject


@HiltViewModel
class FavouriteSongViewModel @Inject constructor(private val appPreferences: AppPreferences,
@ApplicationContext private val context: Context): BaseViewModel() {

    val dataFavSong = MutableLiveData<ArrayList<MediaPlayerSong>>()
    val exception = MutableLiveData<String?>()
    val loading = MutableLiveData<Boolean>()
    init {
        getDataFavSong()
    }

    private fun getDataFavSong() {
        loading.postValue(true)
        appPreferences.getDatabaseReference(Constants.Firebase.FAVOURITE)
            .child(appPreferences.getFirebaseUid())
            .addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    val listSong = ArrayList<MediaPlayerSong>()
                    for (dataSnapshot in snapshot.children ){
                        dataSnapshot.getValue(MediaPlayerSong::class.java)?.let { listSong.add(it) }
                    }
                    dataFavSong.postValue(listSong)
                    loading.postValue(false)
                }

                override fun onCancelled(error: DatabaseError) {
                    exception.postValue(error.toString())
                }

            })
    }
    fun removeFav(name: String){
        appPreferences.getDatabaseReference(Constants.Firebase.FAVOURITE)
            .child(appPreferences.getFirebaseUid())
            .addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (dataSnapshot in snapshot.children ){
                        dataSnapshot.getValue(MediaPlayerSong::class.java)?.let {
                            if (it.name.equals(name)){
                                dataSnapshot.ref.removeValue()
                                exception.postValue(context.getString(R.string.deleted))
                            }
                        }
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    exception.postValue(error.toString())
                }

            })
    }
}