package com.example.baseproject.ui.splash

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.example.baseproject.R
import com.example.baseproject.Ultils.CacheMusic
import com.example.baseproject.databinding.FragmentSplashBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment :
    BaseFragment<FragmentSplashBinding, SplashViewModel>(R.layout.fragment_splash) {

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var cacheMusic: CacheMusic

    private val viewModel: SplashViewModel by viewModels()

    override fun getVM() = viewModel


    override fun bindingAction() {
        super.bindingAction()
        viewModel.actionSPlash.observe(viewLifecycleOwner) {
            if (it is SplashActionState.Finish) {
                appNavigation.openSplashToHomeScreen()
                requestPermission()
            }
        }
    }
    private fun requestPermission() {
        if (Build.VERSION.SDK_INT <= 28) {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    (activity as Activity?)!!,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    Constants.MY_READ_PERMISSION_CODE
                )
            }
        }
    }

}