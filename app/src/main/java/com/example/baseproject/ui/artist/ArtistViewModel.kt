package com.example.baseproject.ui.artist

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.baseproject.R
import com.example.baseproject.Ultils.Contants
import com.example.core.base.BaseViewModel
import com.example.core.database.PlaylistHelper
import com.example.core.model.AlbumDetail
import com.example.core.model.MediaPlayerSong
import com.example.core.model.Song
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArtistViewModel @Inject constructor(
    val savedStateHandle: SavedStateHandle,
    private val repository: ArtistRepository,
    private val dataPlaylist: PlaylistHelper,
    @ApplicationContext val context: Context,
    private val appPreferences: AppPreferences
) : BaseViewModel() {

    private val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
        dataSong.postValue(ArrayList())
    }
    val dataSong = MutableLiveData<ArrayList<AlbumDetail>>()
    val artistImage = MutableLiveData<String>()
    val artistName = MutableLiveData<String>()
    val dataSetup = MutableLiveData<ArrayList<MediaPlayerSong>>()
    val loading = MutableLiveData<Boolean>()
    val exception = MutableLiveData<String?>()

    init {
        getDataArtist()
        getBundle()
    }

    private fun getDataArtist() {
        val keyAlbum: String? = savedStateHandle.get(Contants.ARTIST_ID)
        viewModelScope.launch(Dispatchers.IO + handler) {
            loading.postValue(true)
            try {
                val response = repository.getMusicByArtist()
                val array: ArrayList<AlbumDetail> = ArrayList()
                for (i in response.indices) {
                    if (response[i].artistId.toString() == keyAlbum) {
                        array.add(
                            AlbumDetail(
                                response[i].image,
                                savedStateHandle.get(Contants.ARTIST_NAME),
                                response[i].title,
                                response[i].source,
                                response[i].duration
                            )
                        )
                    }
                }
                val listSetup: ArrayList<MediaPlayerSong> = ArrayList()
                for (i in array.indices) {
                    val duration = array[i].duration!! * 1000
                    val song = MediaPlayerSong(
                        1,
                        array[i].title,
                        array[i].image,
                        array[i].singer,
                        array[i].song,
                        duration
                    )
                    listSetup.add(song)
                }
                dataSetup.postValue(listSetup)
                dataSong.postValue(array)
                loading.postValue(false)
            } catch (e: Exception) {
                Log.getStackTraceString(e)
                loading.postValue(true)
                dataSong.postValue(ArrayList())
            }
        }
    }

    private fun getBundle() {
        artistImage.postValue("" + savedStateHandle.get(Contants.ARTIST_IMAGE))
        artistName.postValue(savedStateHandle.get(Contants.ARTIST_NAME))
    }

    fun insertData(data: AlbumDetail?) {
        var isCheck = false
        if (dataPlaylist.readAllSong().size > 0) {
            for (i in dataPlaylist.readAllSong().indices) {
                if (dataPlaylist.readAllSong()[i].source?.equals(data?.song)!!) {
                    isCheck = true
                }
            }
            if (!isCheck) {
                dataPlaylist.insertSong(
                    Song(
                        data?.song, data?.image, null, data?.title, null, savedStateHandle.get(Contants.ARTIST_ID), null, data?.duration!! *1000
                    )
                )
                exception.postValue(context.getString(R.string.success))
            } else {
                exception.postValue(context.getText(R.string.existed).toString())
            }
        } else {
            dataPlaylist.insertSong(
                Song(
                    data?.song, data?.image, null, data?.title, null, savedStateHandle.get(Contants.ARTIST_ID), null, data?.duration!! *1000
                )
            )
            exception.postValue(context.getString(R.string.success))
        }
    }

    fun insertFav(song: MediaPlayerSong) {
        if (FirebaseAuth.getInstance().currentUser != null) {
            appPreferences.getDatabaseReference(Constants.Firebase.FAVOURITE).child(
                appPreferences
                    .getFirebaseUid()
            ).addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val listSong = ArrayList<MediaPlayerSong>()
                    for (dataSnapshot in snapshot.children) {
                        dataSnapshot.getValue(MediaPlayerSong::class.java)?.let { listSong.add(it) }
                    }
                    if (!listSong.contains(song)) {
                        appPreferences.setSongFirebase(song)
                        exception.postValue(context.getString(R.string.success))
                    } else {
                        exception.postValue(context.getString(R.string.faved))
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    exception.postValue(error.toString())
                }
            })
        } else {
            exception.postValue(context.getString(R.string.login))
        }
    }
}