package com.example.baseproject.ui.tabRank

import com.example.core.model.Song
import com.example.core.model.Artist
import com.example.core.network.RetrofitService
import javax.inject.Inject

class RankRepository @Inject constructor(private val retrofit: RetrofitService){

    suspend fun getTopSong(): ArrayList<Song> = retrofit.getMusic()
    suspend fun getArtist(): ArrayList<Artist> = retrofit.getArtist()
}