package com.example.baseproject.ui.musicLocal

import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import com.example.core.model.SongLocal
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class GetDataSongLocal @Inject constructor(@ApplicationContext private  val context: Context) {
    private val array: ArrayList<SongLocal> = ArrayList()
    init {
        getDataSong()
    }

    private fun getDataSong(){
        val contentResolver: ContentResolver = context.contentResolver
        val songUri: Uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val cursor: Cursor? = contentResolver.query(songUri, null, null, null, null)
        cursor?.let {
            if (it.moveToFirst()) {
                do {
                    val id = it.getLong(it.getColumnIndexOrThrow(MediaStore.Audio.Media._ID))
                    val songUri = ContentUris.withAppendedId(
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        id
                    )
                    val title =
                        it.getString(it.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE))
                    val album =
                        it.getString(it.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM))
                    val albumId =
                        it.getLong(it.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID))
                    val artist =
                        it.getString(it.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST))
                    val artistId =
                        it.getLong(it.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST_ID))
                    val duration =
                        it.getLong(it.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION))
                    val image: String = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        val albumArtUri = Uri.parse("content://media/external/audio/albumart")
                        ContentUris.withAppendedId(albumArtUri, albumId).toString()
                    } else {
                        try {
                            it.getString(it.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART))
                        }catch (exception: IllegalArgumentException) {
                            "https://s3-ap-southeast-1.amazonaws.com/images.spiderum.com/sp-images/ed0ec66099ce11e68814a7e16df546f0.jpg"
                        }
                    }

                    if (!title.equals("")) {
                        val songLocal = SongLocal(
                            id = id,
                            title = title,
                            album = album,
                            albumId = albumId,
                            artist = artist,
                            artistId = artistId,
                            songUri = songUri.toString(),
                            image = image,
                            duration = duration
                        )
                        array.add(songLocal)
                    }
                }while (it.moveToNext())
                returnData()
            }
            it.close()
        }
    }
    fun returnData(): ArrayList<SongLocal>{
        return array
    }
}