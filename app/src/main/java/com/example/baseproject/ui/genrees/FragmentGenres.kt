package com.example.baseproject.ui.genrees

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentGenreesBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.utils.Constants
import com.example.core.utils.isNetworkConnected
import com.example.core.utils.listenner.IOnClickListener
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FragmentGenres: BaseFragment<FragmentGenreesBinding, GenresViewModel>(R.layout.fragment_genrees) {

    private val viewModel: GenresViewModel by viewModels()


    override fun getVM(): GenresViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    private val adapter: GenresAdapter by lazy {
        GenresAdapter()
    }


    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setUpObserver()
        setupAdapter()
    }

    override fun setOnClick() {
        super.setOnClick()
        adapter.setOnItemClickListener(object : IOnClickListener {
            override fun onItemClick(position: Int) {
                if (activity?.isNetworkConnected() == true){
                    val bundle =  Bundle()
                    bundle.putSerializable(Constants.MediaPlayer.PUT_DATA_TO_SERVICE,
                        viewModel.dataSetup.value
                    )
                    bundle.putInt(Constants.MediaPlayer.PUT_POSITION, position)
                    appNavigation.openPlay(bundle)
                }else{
                    Toast.makeText(requireContext(), context?.getString(R.string.connect), Toast.LENGTH_LONG).show()
                }
            }

            override fun insertItem(position: Int) {
                val dialogClickListener =
                    DialogInterface.OnClickListener { _, which ->
                        when (which) {
                            DialogInterface.BUTTON_POSITIVE -> viewModel.insertData(viewModel.dataList.value?.get(position))
                            DialogInterface.BUTTON_NEGATIVE -> {}
                        }
                    }

                val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                builder.setMessage(requireContext().getText(R.string.want_insert)).setPositiveButton(requireContext().getString(R.string.yes), dialogClickListener)
                    .setNegativeButton(requireContext().getString(R.string.no), dialogClickListener).show()
            }

            override fun insertFav(position: Int) {
                viewModel.insertFav(viewModel.dataSetup.value?.get(position)!!)
            }
        })
        binding.btnBackHome.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    private fun setupAdapter() {
        binding.rvAlbumDetail.adapter = adapter
    }

    private fun setUpObserver() {
        viewModel.dataImage.observe(viewLifecycleOwner) {
            Glide.with(requireContext()).load(it).transform(CenterInside(), RoundedCorners(24))
                .into(binding.ivAlbumImg)
        }
        viewModel.dataName.observe(viewLifecycleOwner) {
            binding.tvAlbumName.text = it
        }
        viewModel.dataList.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
        viewModel.loading.observe(viewLifecycleOwner) {
            if (it) {
                binding.pbLoading.visibility = View.VISIBLE
            } else {
                binding.pbLoading.visibility = View.GONE
            }
        }
        viewModel.exception.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

}