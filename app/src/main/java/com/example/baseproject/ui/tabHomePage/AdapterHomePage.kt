package com.example.baseproject.ui.tabHomePage

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.baseproject.R
import com.example.baseproject.databinding.ItemHomePagePagerBinding
import com.example.baseproject.databinding.ItemHomePageRecycleHoriBinding
import com.example.core.model.*
import com.example.core.utils.Constants
import com.example.core.utils.listenner.OnItemClick

class AdapterHomePage : ListAdapter<SectionHome, RecyclerView.ViewHolder>(SectionHomeDiffUtil()) {

    private val sectionAlbum = 1
    private val sectionArtist = 2
    private val sectionSuggest = 3
    private val sectionGenres = 4
    private val sectionPager = 5

    interface OnClickHome{
        fun click(position: Int, dataOf: String)
    }

    private var listener: OnClickHome? = null

    fun setOnItemClickListener(listener: OnClickHome?) {
        this.listener = listener
    }

    interface CommunicationDataBase{
        fun clickUnCare(data: FavouriteArtist)
        fun clickCare(data: FavouriteArtist)
    }

    private var communicate: CommunicationDataBase? = null

    fun setOnClickListener(communicate: CommunicationDataBase){
        this.communicate = communicate
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            sectionAlbum -> {
                AlbumSectionHolder(
                    ItemHomePageRecycleHoriBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                ,listener
                )
            }
            sectionArtist -> {
                ArtistSectionHolder(
                    ItemHomePageRecycleHoriBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    ),
                    listener,
                    communicate
                )
            }
            sectionSuggest -> {
                SuggestSectionHolder(
                    ItemHomePageRecycleHoriBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                ,listener
                )
            }
            sectionGenres -> {
                GenresSectionHolder(
                    ItemHomePageRecycleHoriBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                ,listener
                )
            }

            else -> {
                ViewPagerSectionHolder(
                    ItemHomePagePagerBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                )
            }
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            sectionArtist -> {
                val myViewHolderHead = holder as ArtistSectionHolder
                myViewHolderHead.bindData(getItem(position).listArtist)

            }
            sectionAlbum -> {
                val myViewHolderHead = holder as AlbumSectionHolder
                myViewHolderHead.bindData(getItem(position).listAlbum)
            }
            sectionSuggest -> {
                val myViewHolderHead = holder as SuggestSectionHolder
                myViewHolderHead.bindData(getItem(position).listSuggest)
            }
            sectionGenres -> {
                val myViewHolderHead = holder as GenresSectionHolder
                myViewHolderHead.bindData(getItem(position).listGenrees)
            }
            else -> {
                val myViewHolderHead = holder as ViewPagerSectionHolder
                myViewHolderHead.bindData()
            }
        }

    }


    override fun submitList(list: List<SectionHome>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

    override fun getItemViewType(position: Int): Int {
        when {
            getItem(position).section.equals(Constants.Section.SECTION_ALBUM) -> {
                return sectionAlbum
            }
            getItem(position).section.equals(Constants.Section.SECTION_VIEWPAGER) -> {
                return sectionPager
            }
            getItem(position).section.equals(Constants.Section.SECTION_ARTIST) -> {
                return sectionArtist
            }
            getItem(position).section.equals(Constants.Section.SECTION_SUGGEST) -> {
                return sectionSuggest
            }
            else -> {
                return sectionGenres
            }
        }
    }

}

class ArtistSectionHolder(
    var binding: ItemHomePageRecycleHoriBinding,
    val listener: AdapterHomePage.OnClickHome?,
    val communicationDataBase: AdapterHomePage.CommunicationDataBase?
) : RecyclerView.ViewHolder(binding.root) {

    private val adapterArtist: ArtistAdapter by lazy {
        ArtistAdapter()
    }
    init {
        binding.tvTitle.text = itemView.context.getString(R.string.artist)
        binding.ivIcon.setImageResource(R.drawable.ic__099418_man_singer_icon)
        binding.rvHori.adapter = adapterArtist
        adapterArtist.setOnItemClickListener(object : OnItemClick{
            override fun onItemClick(position: Int) {
                listener?.click(position, Constants.Object.ARTIST)
            }

        })

        adapterArtist.setOnClickListener(object : ArtistAdapter.CommunicationDataBase{
            override fun clickUnCare(data: FavouriteArtist) {
                communicationDataBase?.clickUnCare(data)
            }

            override fun clickCare(data: FavouriteArtist) {
                communicationDataBase?.clickCare(data)
            }

        })

    }

    fun bindData(data: ArrayList<FavouriteArtist>?) {
        adapterArtist.submitList(data)
    }

}

class AlbumSectionHolder(
    var binding: ItemHomePageRecycleHoriBinding,
    listener: AdapterHomePage.OnClickHome?
) : RecyclerView.ViewHolder(binding.root) {
    private val adapterAlbum: SongAdapter by lazy {
        SongAdapter()
    }

    init {
        binding.tvTitle.text = itemView.context.getString(R.string.album)
        binding.ivIcon.setImageResource(R.drawable.ic_music_album_song_icon)
        binding.rvHori.layoutManager = ZoomCenterLinearLayoutManager(itemView.context
            , LinearLayoutManager.HORIZONTAL, false)
        binding.rvHori.adapter = adapterAlbum
        adapterAlbum.setOnItemClickListener(object : OnItemClick{
            override fun onItemClick(position: Int) {
                listener?.click(position, Constants.Object.ALBUM)
            }

        })
    }

    fun bindData(data: ArrayList<Album>?) {
        adapterAlbum.submitList(data)
    }

}

class SuggestSectionHolder(
    var binding: ItemHomePageRecycleHoriBinding
    , listener: AdapterHomePage.OnClickHome?
) : RecyclerView.ViewHolder(binding.root) {
    private val suggestAdapter: SuggestSongAdapter by lazy {
        SuggestSongAdapter()
    }
    init {
        binding.tvTitle.text = itemView.context.getString(R.string.suggest_song)
        binding.ivIcon.setImageResource(R.drawable.ic_check_list_icon)
        binding.rvHori.layoutManager = ZoomCenterLinearLayoutManager(itemView.context
            , LinearLayoutManager.HORIZONTAL, false)
        binding.rvHori.adapter = suggestAdapter
        suggestAdapter.setOnItemClickListener(object : OnItemClick{
            override fun onItemClick(position: Int) {
                listener?.click(position, Constants.Object.SUGGEST)
            }

        })
    }

    fun bindData(data: ArrayList<Song>?) {
        suggestAdapter.submitList(data)
    }

}

class GenresSectionHolder(
    var binding: ItemHomePageRecycleHoriBinding
    , listener: AdapterHomePage.OnClickHome?
) : RecyclerView.ViewHolder(binding.root) {
    private val genresAdapter: GenresAdapter by lazy {
        GenresAdapter()
    }
    init {
        binding.tvTitle.text = itemView.context.getString(R.string.genrees)
        binding.ivIcon.setImageResource(R.drawable.ic_eading_study_icon)
        binding.rvHori.adapter = genresAdapter
        genresAdapter.setOnItemClickListener(object : OnItemClick{
            override fun onItemClick(position: Int) {
                listener?.click(position, Constants.Object.GENREES)
            }

        })
    }

    fun bindData(data: ArrayList<Genrees>?) {
        genresAdapter.submitList(data)
    }

}

class ViewPagerSectionHolder(
    var binding: ItemHomePagePagerBinding
) : RecyclerView.ViewHolder(binding.root), HomePageFragment.RemoveHandler {

    val handler = Handler(Looper.getMainLooper())
    private val runnable = object : Runnable {
        override fun run() {
            var currentItem: Int = binding.vpSilde.currentItem
            currentItem++
            val numPage = binding.vpSilde.adapter?.itemCount ?: 0
            if (currentItem >= numPage) {
                currentItem = 0
            }
            binding.vpSilde.setCurrentItem(currentItem, true)
            handler.postDelayed(this, 5000)
        }
    }

    private val viewPagerAdapter: ViewPagerAdapter by lazy {
        ViewPagerAdapter(itemView.context, ArrayList())
    }

    fun bindData() {
        val listImg: ArrayList<String> = ArrayList()
        listImg.add("https://ieltsrewind.com/wp-content/uploads/2021/01/Describe-your-favorite-singer-image.jpg?ezimgfmt=ng%3Awebp%2Fngcb21%2Frs%3Adevice%2Frscb21-2")
        listImg.add("https://musicapppp.000webhostapp.com/image/dropand.jpg")
        listImg.add("https://file.tinnhac.com/resize/600x-/music/2017/03/29/sontung05-177a.jpg")
        listImg.add("https://avatar-ex-swe.nixcdn.com/playlist/2017/05/31/4/9/7/8/1496224788597_500.jpg")
        binding.vpSilde.adapter = viewPagerAdapter
        viewPagerAdapter.setData(listImg)
        binding.circleIndicator.setViewPager(binding.vpSilde)
        handler.postDelayed(runnable, 5000)

    }

    override fun remove() {
        handler.removeCallbacks(runnable)
    }

}

class SectionHomeDiffUtil : DiffUtil.ItemCallback<SectionHome>() {
    override fun areItemsTheSame(oldItem: SectionHome, newItem: SectionHome): Boolean {
        return oldItem.listAlbum == newItem.listAlbum && oldItem.listArtist == newItem.listArtist
    }

    override fun areContentsTheSame(oldItem: SectionHome, newItem: SectionHome): Boolean {
        return oldItem == newItem
    }

}