package com.example.baseproject.ui.musicLocal

import androidx.lifecycle.MutableLiveData
import com.example.core.base.BaseViewModel
import com.example.core.model.SongLocal
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import com.example.core.model.MediaPlayerSong


@HiltViewModel

class LocalViewModel @Inject constructor(private val getDataSongLocal: GetDataSongLocal): BaseViewModel(){
    val dataLocal = MutableLiveData<ArrayList<SongLocal>>()
    val localPlay = MutableLiveData<ArrayList<MediaPlayerSong>>()

    init {
        getAllListSong()
    }

    private fun getAllListSong() {
        dataLocal.postValue(getDataSongLocal.returnData())
        val arrayList: ArrayList<MediaPlayerSong> = ArrayList()
        for (i in getDataSongLocal.returnData().indices){
            val data = MediaPlayerSong(
                getDataSongLocal.returnData()[i].id?.toInt(),
                getDataSongLocal.returnData()[i].title,
                getDataSongLocal.returnData()[i].image,
                getDataSongLocal.returnData()[i].artist,
                getDataSongLocal.returnData()[i].songUri,
                getDataSongLocal.returnData()[i].duration?.toInt(),
            )
            arrayList.add(data)
        }
        localPlay.postValue(arrayList)
    }

}