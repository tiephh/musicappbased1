package com.example.baseproject.ui.splash

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.example.core.base.BaseViewModel
import com.example.core.model.Users
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.example.core.utils.SingleLiveEvent
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.File
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(private val ref: AppPreferences) : BaseViewModel() {

    val actionSPlash = SingleLiveEvent<SplashActionState>()
    private val handler = Handler(Looper.getMainLooper())

    init {
        if (FirebaseAuth.getInstance().currentUser != null){
            FirebaseDatabase.getInstance().getReference(Constants.Firebase.USER).child(FirebaseAuth.getInstance().currentUser?.uid.toString())
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val users: Users? = snapshot.getValue(Users::class.java)
                    users?.let { ref.setDataUsers(it) }
                }
                override fun onCancelled(error: DatabaseError) {
                }

            })
        }else{
            val users = Users(null, null, null)
            ref.setDataUsers(users)
        }
        handler.postDelayed({
            actionSPlash.value = SplashActionState.Finish
        }, 1000)
    }

    override fun onCleared() {
        handler.removeCallbacksAndMessages(null)
        super.onCleared()
    }

}

sealed class SplashActionState {
    object Finish : SplashActionState()
}