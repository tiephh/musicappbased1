package com.example.baseproject.ui.home

import android.annotation.SuppressLint
import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.view.View
import androidx.fragment.app.viewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentHomeBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.baseproject.service.MediaPlayerService
import com.example.core.base.BaseFragment
import com.example.core.model.MediaPlayerSong
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(R.layout.fragment_home) {

    @Inject
    lateinit var appNavigation: AppNavigation

    private val viewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var ref: AppPreferences

    private var isConnect = false
    private var isStop = true
    @Inject
    lateinit var service: MediaPlayerService
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, mBinder: IBinder) {
            val binder = mBinder as MediaPlayerService.MyBinder
            service = binder.getMediaPlayerService()
            isConnect = true
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            isConnect = false
        }

    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        activity?.bindService(intent, connection, Context.BIND_AUTO_CREATE)
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(mMessageReceiver,
            IntentFilter(Constants.Bundle.SEND_DATA_TO_HOME)
        )
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(receiverStop,
            IntentFilter(Constants.Bundle.STOP_SERVICE_FROM_NOTIFY)
        )
        setupBottomNavigationBar()
    }
    var message: MediaPlayerSong? = null

    private val intent: Intent? by lazy {
        Intent(activity, MediaPlayerService::class.java)
    }

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            isStop = false
            message = intent.getSerializableExtra(Constants.Bundle.SONG) as MediaPlayerSong
            if (message != null){
                binding.ctNavigation.visibility = View.VISIBLE
                binding.tvTitle.text = message!!.name
            }else{
                binding.ctNavigation.visibility = View.GONE
            }
            if (service.isPlaying){
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_baseline_pause_24)
            }else {
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_baseline_play_arrow_24)
            }
        }
    }

    private val receiverStop: BroadcastReceiver = object : BroadcastReceiver() {
        @SuppressLint("SetTextI18n")
        override fun onReceive(context: Context?, intent: Intent) {
            if (intent.getBooleanExtra(Constants.Bundle.STOP, false)){
                binding.ctNavigation.visibility = View.GONE
                isStop = true
            }
        }
    }

    override fun setOnClick() {
        super.setOnClick()
        binding.btnClose.setOnClickListener {
            service.closeMusic()
            activity?.stopService(intent)
            activity?.unbindService(connection)
            isStop = true
            binding.ctNavigation.visibility = View.GONE
        }
        binding.btnPlayOrPause.setOnClickListener {
            if (service.isPlaying){
                isStop = false
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_baseline_play_arrow_24)
                service.pauseMusic()
            }else {
                binding.btnPlayOrPause.setImageResource(R.drawable.ic_baseline_pause_24)
                service.resumeMusic()
            }
        }
        binding.ctNavigation.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable(Constants.Bundle.DATA_PLAYING, message)
            appNavigation.openPlay(bundle)
        }
    }

    private fun setupBottomNavigationBar() {
        val navHostFragment = childFragmentManager.findFragmentById(
            R.id.nav_host_container
        ) as NavHostFragment
        val navController = navHostFragment.navController
        binding.bottomNav.setupWithNavController(navController)
    }

    override fun getVM(): HomeViewModel = viewModel

    override fun onDestroyView() {
        super.onDestroyView()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(mMessageReceiver)
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(receiverStop)
        if (!isStop){
            activity?.unbindService(connection)
        }
    }
}