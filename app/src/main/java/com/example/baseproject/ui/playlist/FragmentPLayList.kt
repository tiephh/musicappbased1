package com.example.baseproject.ui.playlist

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentPlaylistBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.PlaylistHelper
import com.example.core.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class FragmentPLayList: BaseFragment<FragmentPlaylistBinding, PlayListViewModel>(R.layout.fragment_playlist) {

    private val viewModel: PlayListViewModel by viewModels()

    override fun getVM(): PlayListViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var dataPlaylist: PlaylistHelper

    private val adapter: PlayListAdapter by lazy {
        PlayListAdapter()
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupAdapter()
    }

    override fun setOnClick() {
        super.setOnClick()
        adapter.setOnItemClickListener(object : PlayListAdapter.ClickItem{
            override fun clickItem(position: Int) {
                val bundle = Bundle()
                bundle.putSerializable(
                    Constants.MediaPlayer.PUT_DATA_TO_SERVICE,
                    viewModel.dataPlaylist.value
                )
                bundle.putInt(Constants.MediaPlayer.PUT_POSITION, position)
                appNavigation.openPlay(bundle)
            }

            override fun clickMenu(position: Int) {
                val dialogClickListener =
                    DialogInterface.OnClickListener { _, which ->
                        when (which) {
                            DialogInterface.BUTTON_POSITIVE -> {
                                dataPlaylist.deleteSong(dataPlaylist.readAllSong()[position].id.toString())
                                viewModel.readPlaylist()
                            }
                            DialogInterface.BUTTON_NEGATIVE -> {}
                        }
                    }

                val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                builder.setMessage(requireContext().getText(R.string.want_delete)).setPositiveButton(requireContext().getString(R.string.yes), dialogClickListener)
                    .setNegativeButton(requireContext().getString(R.string.no), dialogClickListener).show()

            }
        })
        binding.btnBackHome.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    private fun setupAdapter() {
        binding.rvPlayList.adapter = adapter
        viewModel.dataPlaylist.observe(viewLifecycleOwner){
            adapter.submitList(it)
        }
    }
}