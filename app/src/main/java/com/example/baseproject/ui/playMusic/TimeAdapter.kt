package com.example.baseproject.ui.playMusic

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.databinding.ItemSuggestSongBinding
import com.example.baseproject.databinding.ItemTimeBinding
import com.example.core.model.SongLocal
import com.example.core.model.Time
import com.example.core.utils.listenner.OnItemClick

class TimeAdapter : ListAdapter<Time, TimeHolder>(TimeDiffUtil()){

    private var listener: OnItemClick? = null

    fun setOnItemClickListener(listener: OnItemClick?) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeHolder {
        return TimeHolder(
            ItemTimeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ),
            listener

        )
    }

    override fun onBindViewHolder(holder: TimeHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<Time>?) {
        super.submitList(list?.let { ArrayList(it) })
    }


}

class TimeHolder(
    var binding: ItemTimeBinding
    , listener: OnItemClick?
) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.item.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
    }

    fun bindData(data: Time) {
        binding.tvTime.text = data.name
    }

}
class TimeDiffUtil : DiffUtil.ItemCallback<Time>() {
    override fun areItemsTheSame(oldItem: Time, newItem: Time): Boolean {
        return oldItem.time == newItem.time && oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: Time, newItem: Time): Boolean {
        return oldItem == newItem
    }

}