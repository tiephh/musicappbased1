package com.example.baseproject.ui.personal


import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.PopupMenu
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.baseproject.R
import com.example.baseproject.Ultils.Contants
import com.example.baseproject.databinding.DialogLanguageBinding
import com.example.baseproject.databinding.FragmentUserBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.FavouriteHelper
import com.example.core.model.FavouriteArtist
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@Suppress("DEPRECATION")
@AndroidEntryPoint
class FragmentUser: BaseFragment<FragmentUserBinding, UserViewModel>(R.layout.fragment_user) {

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var favouriteHelper: FavouriteHelper

    @Inject
    lateinit var reference: AppPreferences

    private val viewModel: UserViewModel by viewModels()

    private val adapterFavourite: AdapterFavourite by lazy {
        AdapterFavourite()
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                appNavigation.openUserToLocal()
            } else {
                Toast.makeText(requireContext(), requireContext().getString(R.string.permission), Toast.LENGTH_LONG).show()
            }
        }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupRecycle()
        setupView()
    }

    private fun setupView() {
        binding.constraintLayout.setBackgroundResource(R.drawable.anh)
        if (FirebaseAuth.getInstance().currentUser != null){
            binding.tvName.text = reference.getDataUser().username
            binding.tvPopup.visibility = View.GONE
            if (reference.getDataUser().image.equals(Constants.Firebase.DEFAULT)){
                binding.ivAvatar.setImageResource(R.drawable.ic_launcher_foreground)
            }else{
                binding.pbImgLoading.visibility = View.VISIBLE
                Glide.with(requireContext()).load(reference.getDataUser().image)
                    .listener(object : RequestListener<Drawable>{
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }
                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            binding.pbImgLoading.visibility = View.GONE
                           return false
                        }
                    })
                    .circleCrop().into(binding.ivAvatar)
            }
        }else{
            binding.ivAvatar.setImageResource(R.drawable.ic_launcher_foreground)
            binding.tvName.visibility = View.GONE
            binding.tvPopup.visibility = View.VISIBLE
        }
    }

    private fun setupRecycle() {
        if(favouriteHelper.readAllFavourite().size == 0){
            binding.ivNoneNullFavourite.visibility = View.VISIBLE
        }else{
            binding.ivNoneNullFavourite.visibility = View.GONE
        }
        binding.rvFavourite.adapter = adapterFavourite
        adapterFavourite.submitList(favouriteHelper.readAllFavourite())
    }

    override fun getVM(): UserViewModel = viewModel


    override fun setOnClick() {
        super.setOnClick()
        binding.ivLibrary.setOnClickListener {
            if (Build.VERSION.SDK_INT <= 28) {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        (activity as Activity?)!!,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        Constants.MY_READ_PERMISSION_CODE
                    )
                    requestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
                }else {
                    appNavigation.openUserToLocal()
                }
            }else {
                appNavigation.openUserToLocal()
            }
        }
        binding.ivPlayList.setOnClickListener {
            appNavigation.openHomeToPlaylist()
        }

        adapterFavourite.setOnItemClickListener(object : AdapterFavourite.OnClickArtist{
            override fun itemClick(data: FavouriteArtist) {
                val bundle = Bundle()
                bundle.putString(Contants.ARTIST_ID, data.idFav.toString())
                bundle.putString(Contants.ARTIST_IMAGE, data.image)
                bundle.putString(Contants.ARTIST_NAME, data.name)
                appNavigation.openHomePageArtist(bundle)
            }

        })

        val popupMenu = PopupMenu(requireContext(), binding.btnMenu)
        popupMenu.menuInflater.inflate(R.menu.menu_popup_user, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener,
            PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                if (item.itemId == R.id.menu_login){
                    if (FirebaseAuth.getInstance().currentUser != null){
                        val dialogClickListener =
                            DialogInterface.OnClickListener { _, which ->
                                when (which) {
                                    DialogInterface.BUTTON_POSITIVE -> {
                                        viewModel.logout()
                                    }
                                    DialogInterface.BUTTON_NEGATIVE -> {}
                                }
                            }

                        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                        builder.setMessage(requireContext().getText(R.string.want_logout))
                            .setPositiveButton(requireContext().getString(R.string.yes), dialogClickListener)
                            .setNegativeButton(requireContext().getString(R.string.no), dialogClickListener).show()
                    }
                    appNavigation.openToLogin()
                }else if (item.itemId == R.id.menu_register){
                    if (FirebaseAuth.getInstance().currentUser != null){
                        val dialogClickListener =
                            DialogInterface.OnClickListener { _, which ->
                                when (which) {
                                    DialogInterface.BUTTON_POSITIVE -> {
                                        viewModel.logout()
                                    }
                                    DialogInterface.BUTTON_NEGATIVE -> {}
                                }
                            }

                        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                        builder.setMessage(requireContext().getText(R.string.want_logout))
                            .setPositiveButton(requireContext().getString(R.string.yes), dialogClickListener)
                            .setNegativeButton(requireContext().getString(R.string.no), dialogClickListener).show()
                    }
                    appNavigation.openToRegister()
                }else{
                    if (FirebaseAuth.getInstance().currentUser != null){
                        val dialogClickListener =
                            DialogInterface.OnClickListener { _, which ->
                                when (which) {
                                    DialogInterface.BUTTON_POSITIVE -> {
                                        viewModel.logout()
                                        val intent = requireActivity().intent
                                        requireActivity().finish()
                                        startActivity(intent)
                                    }
                                    DialogInterface.BUTTON_NEGATIVE -> {
                                    }
                                }
                            }

                        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                        builder.setMessage(requireContext().getText(R.string.want_logout))
                            .setPositiveButton(requireContext().getString(R.string.yes), dialogClickListener)
                            .setNegativeButton(requireContext().getString(R.string.no), dialogClickListener).show()
                    }else{
                        Toast.makeText(requireContext(),requireContext().getString(R.string.none_acc), Toast.LENGTH_LONG).show()
                    }
                }
                return true
            }
        })

        binding.btnMenu.setOnClickListener {
            popupMenu.show()
        }

        binding.ivMultiLaguage.setOnClickListener {
            dialog()
        }
        binding.tvPopup.setOnClickListener {
            appNavigation.openToLogin()
        }

        binding.ivFavSong.setOnClickListener {
            appNavigation.openFavourite()
        }
        binding.ivAvatar.setOnClickListener {
           if (FirebaseAuth.getInstance().currentUser != null){
               val intent = Intent()
               intent.type = "image/*"
               intent.action = Intent.ACTION_GET_CONTENT
               startActivityForResult(intent, Constants.NetworkRequestCode.REQUEST_CODE_400)
           }else{
               Toast.makeText(requireContext(), requireContext().getString(R.string.none_acc), Toast.LENGTH_LONG).show()
           }
        }
    }
    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode ==  Constants.NetworkRequestCode.REQUEST_CODE_400 &&
            resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            if (data.data != null) {
                if (context != null) {
                    if (data.data.toString() == Constants.Firebase.DEFAULT)
                        Glide.with(requireContext()).
                        load(R.mipmap.ic_launcher).into(binding.ivAvatar)
                    else {
                        Glide.with(requireContext()).load(data.data.toString())
                            .circleCrop().into(binding.ivAvatar)
                    }
                    viewModel.changeImage(data.data!!)
                }
            }
        }
    }

    private fun dialog() {
        val dialog = Dialog(requireActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val bindingDialog: DialogLanguageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(
                context
            ), R.layout.dialog_language, null, false
        )
        dialog.setContentView(bindingDialog.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        bindingDialog.btnCancel.setOnClickListener {
            dialog.dismiss()
        }

        val sharedPref = requireActivity().getSharedPreferences(Constants.PREF_LANGUAGE, Context.MODE_PRIVATE)
        val myLanguage = sharedPref?.getString("language1", "")

        if (myLanguage.equals("vi")) {
            bindingDialog.cbVI.isChecked = true
            bindingDialog.cbEN.isChecked = false
        } else {
            bindingDialog.cbVI.isChecked = false
            bindingDialog.cbEN.isChecked = true
        }

        bindingDialog.cbEN.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                bindingDialog.cbVI.isChecked = false
            }
        }

        bindingDialog.cbVI.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                bindingDialog.cbEN.isChecked = false
            }
        }

        bindingDialog.btnSubmit.setOnClickListener {
            val sharedPref = requireActivity().getSharedPreferences(Constants.PREF_LANGUAGE, Context.MODE_PRIVATE)
            if (sharedPref != null) {
                if (bindingDialog.cbVI.isChecked && !bindingDialog.cbEN.isChecked)
                    with(sharedPref.edit()) {
                        putString("language1", "vi")
                        apply()
                    } else {
                    with(sharedPref.edit()) {
                        putString("language1", "en")
                        apply()
                    }
                }
                activity?.recreate()
            }
        }
        dialog.show()
    }
}