package com.example.baseproject.ui.album

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentAlbumBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import com.example.core.utils.Constants
import com.example.core.utils.listenner.IOnClickListener


@AndroidEntryPoint
class FragmentAlbum :
    BaseFragment<FragmentAlbumBinding, FragmentAlbumViewModel>(R.layout.fragment_album) {

    private val viewModel: FragmentAlbumViewModel by viewModels()

    override fun getVM(): FragmentAlbumViewModel = viewModel

    private val adapter: AlbumDetailAdapter by lazy {
        AlbumDetailAdapter()
    }
    @Inject
    lateinit var appNavigation: AppNavigation


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObserver()
        init()
        setupAdapter()
    }


    private fun setupAdapter() {
        binding.rvAlbumDetail.adapter = adapter
        adapter.setOnItemClickListener(object : IOnClickListener {
            override fun onItemClick(position: Int) {
                    val bundle = Bundle()
                    bundle.putSerializable(Constants.MediaPlayer.PUT_DATA_TO_SERVICE,
                        viewModel.dataSetup.value
                    )
                    bundle.putInt(Constants.MediaPlayer.PUT_POSITION, position)
                    appNavigation.openPlay(bundle)
            }

            override fun insertItem(position: Int) {
                val dialogClickListener =
                    DialogInterface.OnClickListener { _, which ->
                        when (which) {
                            DialogInterface.BUTTON_POSITIVE -> viewModel.insertData(viewModel.dataSong.value?.get(position))
                            DialogInterface.BUTTON_NEGATIVE -> {}
                        }
                    }

                val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                builder.setMessage(requireContext().getText(R.string.want_insert)).setPositiveButton(requireContext().getString(R.string.yes), dialogClickListener)
                    .setNegativeButton(requireContext().getString(R.string.no), dialogClickListener).show()
            }

            override fun insertFav(position: Int) {
                viewModel.insertFav(viewModel.dataSetup.value?.get(position)!!)
            }

        })
    }


    private fun init() {
        binding.btnBackHome.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupObserver() {
        viewModel.dataSong.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
        viewModel.albumName.observe(viewLifecycleOwner) {
            binding.tvAlbumName.text = it
        }
        viewModel.albumImage.observe(viewLifecycleOwner) {
            Glide.with(requireActivity()).load(it).transform(CenterInside(), RoundedCorners(24))
                .into(binding.ivAlbumImg)
        }
        viewModel.albumTotal.observe(
            viewLifecycleOwner,
        ) {
            binding.tvTotalAlbum.text = context?.getString(R.string.total) + it
        }
        viewModel.loading.observe(viewLifecycleOwner) {
            if (it) {
                binding.pbLoading.visibility = View.VISIBLE
            } else {
                binding.pbLoading.visibility = View.GONE
            }
        }
        viewModel.exception.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }


}