package com.example.baseproject.ui.favoriteSong

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.baseproject.R
import com.example.baseproject.databinding.FavouriteSongFragmentBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class FavouriteSongFragment: BaseFragment<FavouriteSongFragmentBinding, FavouriteSongViewModel>(R.layout.favourite_song_fragment) {

    private val viewModel: FavouriteSongViewModel by viewModels()

    private val adapter: FavSongAdapter by lazy {
        FavSongAdapter()
    }

    @Inject
    lateinit var appNavigation: AppNavigation

    override fun getVM(): FavouriteSongViewModel = viewModel

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setupRecycleView()
        setupObserver()
    }

    override fun setOnClick() {
        super.setOnClick()
        binding.btnBackHome.setOnClickListener { 
            Navigation.findNavController(requireView()).navigateUp()
        }

        adapter.setOnItemClickListener(object : FavSongAdapter.IOnClickListener{
            override fun onItemClick(position: Int) {
                val bundle = Bundle()
                bundle.putSerializable(
                    Constants.MediaPlayer.PUT_DATA_TO_SERVICE,
                    viewModel.dataFavSong.value
                )
                bundle.putInt(Constants.MediaPlayer.PUT_POSITION, position)
                appNavigation.openPlay(bundle)
            }

            override fun deleteItem(position: Int) {
                val dialogClickListener =
                    DialogInterface.OnClickListener { _, which ->
                        when (which) {
                            DialogInterface.BUTTON_POSITIVE -> {
                                viewModel.dataFavSong.value?.get(position)?.let { viewModel.removeFav(
                                    it.name.toString()
                                ) }
                            }
                            DialogInterface.BUTTON_NEGATIVE -> {}
                        }
                    }

                val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                builder.setMessage(requireContext().getText(R.string.want_insert)).setPositiveButton(requireContext().getString(R.string.yes), dialogClickListener)
                    .setNegativeButton(requireContext().getString(R.string.no), dialogClickListener).show()
            }

        })
    }

    private fun setupRecycleView() {
        binding.rvFavSong.adapter = adapter
    }

    private fun setupObserver() {
        viewModel.dataFavSong.observe(viewLifecycleOwner) {
            if (it.size == 0) {
                binding.ivNothing.visibility = View.VISIBLE
            } else {
                adapter.submitList(it)
                binding.ivNothing.visibility = View.GONE
            }
        }
        viewModel.exception.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG)
                .show()
        }
        viewModel.loading.observe(viewLifecycleOwner) {
            if (it) {
                binding.pbLoading.visibility = View.VISIBLE
            } else {
                binding.pbLoading.visibility = View.GONE
            }
        }
    }
}