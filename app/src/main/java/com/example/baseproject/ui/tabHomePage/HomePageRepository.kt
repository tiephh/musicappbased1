package com.example.baseproject.ui.tabHomePage

import com.example.core.model.Album
import com.example.core.model.Genrees
import com.example.core.model.Song
import com.example.core.model.Artist
import com.example.core.network.RetrofitService
import javax.inject.Inject

class HomePageRepository @Inject constructor(private val retrofit: RetrofitService) {

    suspend fun getAlbum(): ArrayList<Album> = retrofit.getAlbum()
    suspend fun getArtist(): ArrayList<Artist> = retrofit.getArtist()
    suspend fun getSong(): ArrayList<Song> = retrofit.getTopSong()
    suspend fun getGenres(): ArrayList<Genrees> = retrofit.getGenres()
}