package com.example.baseproject.ui.tabHomePage

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.core.utils.listenner.OnItemClick
import com.example.baseproject.databinding.ItemArtistBinding
import com.example.core.database.FavouriteHelper
import com.example.core.model.FavouriteArtist

class ArtistAdapter: ListAdapter<FavouriteArtist, ArtistHolder>(ArtistDiffUtil()){


    private var listener: OnItemClick? = null

    fun setOnItemClickListener(listener: OnItemClick?) {
        this.listener = listener
    }

    interface CommunicationDataBase{
        fun clickUnCare(data: FavouriteArtist)
        fun clickCare(data: FavouriteArtist)
    }

    private var communicate: CommunicationDataBase? = null

    fun setOnClickListener(communicate: CommunicationDataBase){
        this.communicate = communicate
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistHolder {
        return ArtistHolder(
            ItemArtistBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ), listener,
        communicate)
    }

    override fun onBindViewHolder(holder: ArtistHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<FavouriteArtist>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

}

class ArtistHolder(
    var binding: ItemArtistBinding
    , listener: OnItemClick?,
    private val communicate: ArtistAdapter.CommunicationDataBase?
) : RecyclerView.ViewHolder(binding.root) {

    val favourite = FavouriteHelper(itemView.context)

    init {
        binding.tvNameArtist.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
        binding.ivImageArtist.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
    }

    fun bindData(data: FavouriteArtist) {
        binding.tvNameArtist.text = data.name
        Glide.with(itemView.context).load(data.image).centerCrop().into(binding.ivImageArtist)
        if (data.isFavourite == 1){
            binding.btnCareArtist.visibility = View.GONE
            binding.btnUnCareArtist.visibility = View.VISIBLE
        }else{
            binding.btnCareArtist.visibility = View.VISIBLE
            binding.btnUnCareArtist.visibility = View.GONE
        }
        binding.btnUnCareArtist.setOnClickListener {
            communicate?.clickUnCare(data)
            binding.btnCareArtist.visibility = View.GONE
        }
        binding.btnCareArtist.setOnClickListener {
            communicate?.clickCare(data)
            binding.btnUnCareArtist.visibility = View.GONE
        }
    }

}
class ArtistDiffUtil : DiffUtil.ItemCallback<FavouriteArtist>() {
    override fun areItemsTheSame(oldItem: FavouriteArtist, newItem: FavouriteArtist): Boolean {
        return oldItem.name == newItem.name && oldItem.id == newItem.id && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(oldItem: FavouriteArtist, newItem: FavouriteArtist): Boolean {
        return oldItem == newItem
    }

}
