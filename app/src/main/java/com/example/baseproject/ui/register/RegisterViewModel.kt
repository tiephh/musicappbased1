package com.example.baseproject.ui.register

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.core.base.BaseViewModel
import com.example.core.model.Users
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class RegisterViewModel @Inject constructor(private val reference: AppPreferences): BaseViewModel() {

    val resultData = MutableLiveData<Boolean>()
    val auth: FirebaseAuth = Firebase.auth
    fun register(email: String, pass: String, username: String){
        auth.createUserWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = Firebase.auth.currentUser
                    user?.let {
                        val uid = user.uid
                        val hashMap:HashMap<String,String> = HashMap<String,String>()
                        hashMap[Constants.Firebase.ID] = uid
                        hashMap[Constants.Firebase.USERNAME] = username
                        hashMap[Constants.Firebase.IMAGE] = Constants.Firebase.DEFAULT
                        FirebaseDatabase.getInstance().getReference(Constants.Firebase.USER).child(uid).setValue(hashMap).addOnCompleteListener {
                                task ->
                            if(task.isSuccessful){
                                FirebaseDatabase.getInstance().getReference(Constants.Firebase.USER).child(uid)
                                    .addValueEventListener(object : ValueEventListener{
                                        override fun onDataChange(snapshot: DataSnapshot) {
                                            val users: Users? = snapshot.getValue(Users::class.java)
                                            if (users != null) {
                                                reference.setDataUsers(users)
                                            }
                                        }
                                        override fun onCancelled(error: DatabaseError) {
                                        }

                                    })
                                login(email, pass)
                            }
                        }
                    }
                } else {
                }
            }
    }
    fun login(email: String, pass: String){
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    resultData.postValue(true)
                } else {
                    resultData.postValue(false)
                }
            }
    }
}