package com.example.baseproject.ui.album

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.baseproject.R
import com.example.baseproject.Ultils.Contants
import com.example.core.base.BaseViewModel
import com.example.core.database.PlaylistHelper
import com.example.core.model.AlbumDetail
import com.example.core.model.MediaPlayerSong
import com.example.core.model.Song
import com.example.core.pref.AppPreferences
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class FragmentAlbumViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val albumRepository: AlbumRepository,
    private val appPreferences: AppPreferences,
    private val dataPlaylist: PlaylistHelper,
    @ApplicationContext val context: Context
) : BaseViewModel() {

    val dataSong = MutableLiveData<ArrayList<AlbumDetail>>()
    val albumImage = MutableLiveData<String>()
    val albumName = MutableLiveData<String>()
    val albumTotal = MutableLiveData<String>()
    val dataSetup = MutableLiveData<ArrayList<MediaPlayerSong>>()
    val loading = MutableLiveData<Boolean>()
    val exception = MutableLiveData<String?>()
    private var idArtist: Int? = null
    private val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    init {
        getDataAlbum()
    }

    private fun getDataAlbum() {
        val keyAlbum: String? = savedStateHandle.get(Contants.ALBUM_ID)
        viewModelScope.launch(Dispatchers.IO + handler) {
            loading.postValue(true)
            try {
                val response1 = async { albumRepository.getDataAlbumByKey() }
                val response2 = async { albumRepository.getDataSinger() }
                val albumImgRepo: Deferred<String?> =
                    async { savedStateHandle.get(Contants.ALBUM_IMAGE) }
                val albumNameRepo: Deferred<String?> =
                    async { savedStateHandle.get(Contants.AlBUM_TITLE) }
                val albumTotalResp: Deferred<String?> =
                    async { savedStateHandle.get(Contants.ALBUM_TOTAL_SONG) }

                val list = async {
                    var singerName: String? = ""
                    for (i in response2.await().indices) {
                        if (response2.await()[i].id.toString() == keyAlbum) {
                            singerName = response2.await()[i].name
                            idArtist = response2.await()[i].id
                        }
                    }
                    val array: ArrayList<AlbumDetail> = ArrayList()
                    for (i in response1.await().indices) {
                        if (response1.await()[i].albumId == keyAlbum?.toInt()) {
                            array.add(
                                AlbumDetail(
                                    response1.await()[i].image,
                                    singerName,
                                    response1.await()[i].title,
                                    response1.await()[i].source,
                                    response1.await()[i].duration
                                )
                            )
                        }
                    }
                    array
                }

                val response = awaitAll(list, albumImgRepo, albumNameRepo, albumTotalResp)

                val data = response[0] as ArrayList<AlbumDetail>?
                val listSetup: ArrayList<MediaPlayerSong> = ArrayList()
                for (i in data?.indices!!) {
                    val song = MediaPlayerSong(
                        1,
                        data[i].title,
                        data[i].image,
                        data[i].singer,
                        data[i].song,
                        data[i].duration!! * 1000
                    )
                    listSetup.add(song)
                }
                dataSetup.postValue(listSetup)
                albumImage.postValue(response[1] as String?)
                albumName.postValue(response[2] as String?)
                albumTotal.postValue(response[3] as String?)
                dataSong.postValue(response[0] as ArrayList<AlbumDetail>?)

                loading.postValue(false)
            } catch (e: Exception) {
                loading.postValue(true)
                Log.getStackTraceString(e)
                dataSong.postValue(ArrayList())
                albumImage.postValue(String())
                albumName.postValue(String())
                albumTotal.postValue(String())
            }
        }
    }

    fun insertData(data: AlbumDetail?) {
        var isCheck = false
        if (dataPlaylist.readAllSong().size > 0) {
            for (i in dataPlaylist.readAllSong().indices) {
                if (dataPlaylist.readAllSong()[i].source?.equals(data?.song)!!) {
                    isCheck = true
                }
            }
            if (!isCheck) {
                dataPlaylist.insertSong(
                    Song(
                        data?.song, data?.image, null, data?.title, null, idArtist, null, data?.duration!! * 1000
                    )
                )
                exception.postValue(context.getString(R.string.success))
            } else {
                messageError.postValue(context.getText(R.string.existed).toString())
            }
        } else {
            dataPlaylist.insertSong(
                Song(
                    data?.song, data?.image, null, data?.title, null, idArtist, null, data?.duration!! * 1000
                )
            )
            exception.postValue(context.getString(R.string.success))
        }
    }

    fun insertFav(song: MediaPlayerSong) {
        if (FirebaseAuth.getInstance().currentUser != null) {
            appPreferences.getDatabaseReference(Constants.Firebase.FAVOURITE).child(
                appPreferences
                    .getFirebaseUid()
            ).addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val listSong = ArrayList<MediaPlayerSong>()
                    for (dataSnapshot in snapshot.children) {
                        dataSnapshot.getValue(MediaPlayerSong::class.java)?.let { listSong.add(it) }
                    }
                    if (!listSong.contains(song)) {
                        appPreferences.setSongFirebase(song)
                        exception.postValue(context.getString(R.string.success))
                    } else {
                        exception.postValue(context.getString(R.string.faved))
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    exception.postValue(error.toString())
                }
            })
        } else {
            exception.postValue(context.getString(R.string.login))
        }
    }
}