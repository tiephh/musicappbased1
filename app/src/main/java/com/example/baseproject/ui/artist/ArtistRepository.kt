package com.example.baseproject.ui.artist

import com.example.core.model.Song
import com.example.core.network.RetrofitService
import javax.inject.Inject

class ArtistRepository @Inject constructor(private val retrofit: RetrofitService) {

    suspend fun getMusicByArtist(): ArrayList<Song> = retrofit.getMusic()
}