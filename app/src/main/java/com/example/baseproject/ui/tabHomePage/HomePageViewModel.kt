package com.example.baseproject.ui.tabHomePage


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.core.base.BaseViewModel
import com.example.core.database.FavouriteHelper
import com.example.core.model.*
import com.example.core.model.Artist
import com.example.core.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class HomePageViewModel @Inject constructor(private val repository: HomePageRepository, private val favourite: FavouriteHelper) : BaseViewModel() {
    val dataSong = MutableLiveData<ArrayList<Album>>()
    val dataArtist = MutableLiveData<ArrayList<Artist>>()
    val dataSuggest = MutableLiveData<ArrayList<Song>>()
    val dataGenrees = MutableLiveData<ArrayList<Genrees>>()
    val dataHome = MutableLiveData<ArrayList<SectionHome>>()
    val dataPlay = MutableLiveData<ArrayList<MediaPlayerSong>>()
    val loading = MutableLiveData<Boolean>()

    private val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
        dataSong.postValue(ArrayList())
        dataArtist.postValue(ArrayList())
        dataSuggest.postValue(ArrayList())
        dataGenrees.postValue(ArrayList())
        isLoading.postValue(false)
    }

    init {
        getListSong()
    }


    fun getListSong() {
        Log.d("check", favourite.readAllFavourite().size.toString())

        viewModelScope.launch(Dispatchers.IO + handler ) {
            loading.postValue(true)
            val song = async { repository.getAlbum() }
            val artist = async { repository.getArtist() }
            val suggest = async { repository.getSong() }
            val genres = async { repository.getGenres() }

            val list = async {
                val arrayMedia: ArrayList<MediaPlayerSong> = ArrayList()
                for (i in suggest.await().indices){
                    var singer = ""
                    for (j in artist.await().indices){
                        if(suggest.await().get(i).artistId?.equals(artist.await().get(j).id) == true){
                            singer = artist.await()[j].name.toString()
                        }
                    }
                    val data = MediaPlayerSong(1,
                        suggest.await()[i].title,
                        suggest.await()[i].image,
                        singer,
                        suggest.await()[i].source,
                        suggest.await()[i].duration!! * 1000)
                    arrayMedia.add(data)
                }
                arrayMedia
            }

            val response = awaitAll(song, artist, suggest, genres, list)
            val listFavourite: ArrayList<FavouriteArtist> = ArrayList()
            val data = response[1] as ArrayList<Artist>
            for (i in data.indices){
                var isFavourite = false
                var id: Int? = null
                for (j in favourite.readAllFavourite().indices){
                    if (data[i].name.equals(favourite.readAllFavourite()[j].name)){
                        isFavourite = true
                        id = favourite.readAllFavourite()[j].id
                    }
                }
                if (isFavourite){
                    listFavourite.add(FavouriteArtist(data[i].name, data[i].image, id, 1, data[i].id))
                }else{
                    listFavourite.add(FavouriteArtist(data[i].name, data[i].image, null, 2, data[i].id))
                }
            }
            dataPlay.postValue(response[4] as ArrayList<MediaPlayerSong>?)
            dataSong.postValue(response[0] as ArrayList<Album>?)
            dataArtist.postValue(response[1] as ArrayList<Artist>?)
            dataSuggest.postValue(response[2] as ArrayList<Song>?)
            dataGenrees.postValue(response[3] as ArrayList<Genrees>?)

            val array: ArrayList<SectionHome> = ArrayList()
            array.add(SectionHome(null, null, null, null ,Constants.Section.SECTION_VIEWPAGER))
            array.add(SectionHome(response[0] as ArrayList<Album>?, null, null, null ,Constants.Section.SECTION_ALBUM))
            array.add(SectionHome(null, listFavourite, null, null ,Constants.Section.SECTION_ARTIST))
            array.add(SectionHome(null, null, response[2] as ArrayList<Song>?, null ,Constants.Section.SECTION_SUGGEST))
            array.add(SectionHome(null, null, null, response[3] as ArrayList<Genrees>? ,Constants.Section.SECTION_GENREES))


            dataHome.postValue(array)
            loading.postValue(false)

        }
    }
}
