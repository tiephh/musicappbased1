package com.example.baseproject.ui.tabRank

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.baseproject.R
import com.example.baseproject.databinding.FragmentRankBinding
import com.example.baseproject.navigation.AppNavigation
import com.example.core.base.BaseFragment
import com.example.core.database.PlaylistHelper
import com.example.core.model.AlbumDetail
import com.example.core.model.Song
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import android.content.DialogInterface
import android.widget.Toast
import com.example.core.utils.Constants


@AndroidEntryPoint
class RankFragment :
    BaseFragment<FragmentRankBinding, RankViewModel>(R.layout.fragment_rank) {

    private val viewModel: RankViewModel by viewModels()

    override fun getVM(): RankViewModel = viewModel

    @Inject
    lateinit var appNavigation: AppNavigation

    @Inject
    lateinit var dataPlaylist: PlaylistHelper

    private val adapter: TopSongAdapter by lazy {
        TopSongAdapter(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        setupObserver()
    }

    private fun setupAdapter() {
        binding.rvTopSong.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rvTopSong.adapter = adapter
        adapter.setOnItemClickListener(object : TopSongAdapter.OnItemClick {
            override fun onItemClick(position: Int) {
                val bundle = Bundle()
                bundle.putSerializable(Constants.MediaPlayer.PUT_DATA_TO_SERVICE,
                    viewModel.dataSetup.value
                )
                bundle.putInt(Constants.MediaPlayer.PUT_POSITION, position)
                appNavigation.openPlay(bundle)
            }

            override fun onItemMenu(position: Int) {
                val dialogClickListener =
                    DialogInterface.OnClickListener { _, which ->
                        when (which) {
                            DialogInterface.BUTTON_POSITIVE -> insertData(viewModel.dataTop.value?.get(position))
                            DialogInterface.BUTTON_NEGATIVE -> {}
                        }
                    }

                val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                builder.setMessage(requireContext().getText(R.string.want_insert)).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show()
            }
        })
    }

    private fun insertData(data: AlbumDetail?){
        var isCheck = false
        if(dataPlaylist.readAllSong().size > 0){
            for (i in dataPlaylist.readAllSong().indices){
                if (!dataPlaylist.readAllSong()[i].source?.equals(data?.song)!!){
                    isCheck = true
                }
            }
            if (isCheck){
                dataPlaylist.insertSong(
                    Song(data?.song, data?.image, null,data?.title
                        , null, null,null, data?.duration)
                )
            }else{
                Toast.makeText(requireContext(), requireContext().getText(R.string.existed), Toast.LENGTH_LONG).show()
            }
        }else {
            dataPlaylist.insertSong(
                Song(
                    data?.song, data?.image, null, data?.title, null, null, null, data?.duration
                )
            )
        }
    }


    private fun setupObserver() {
        viewModel.dataTop.observe(viewLifecycleOwner) {
            viewModel.isLoading.value = false
            adapter.submitList(it)
        }
        viewModel.loading.observe(viewLifecycleOwner) {
            if (it) {
                binding.pbLoading.visibility = View.VISIBLE
            } else {
                binding.pbLoading.visibility = View.GONE
            }
        }
    }

    
}