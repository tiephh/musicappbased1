package com.example.baseproject.ui.tabHomePage

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.baseproject.databinding.ItemAlbumBinding
import com.example.core.model.Song
import com.example.core.utils.listenner.OnItemClick

class SuggestSongAdapter : ListAdapter<Song, SuggestSongHolder>(SuggestDiffUtil()){

    var listener: OnItemClick? = null

    fun setOnItemClickListener(listener: OnItemClick) {
        this.listener = listener
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuggestSongHolder {
        return SuggestSongHolder(
            ItemAlbumBinding.inflate(
                LayoutInflater.from(parent.context), parent, false)
            , listener
            )
    }

    override fun onBindViewHolder(holder: SuggestSongHolder, position: Int) {
        holder.bindData(getItem(position))
    }


    override fun submitList(list: List<Song>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).title.hashCode().toLong()
    }

}

class SuggestSongHolder(
    var binding: ItemAlbumBinding
    , listener: OnItemClick?
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.itemAlbum.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
    }

    fun bindData(data: Song) {
        Glide.with(itemView.context).load(data.image).circleCrop().into(binding.ivImgAlbum)
        binding.tvAlbumName.text = data.title
    }

}
class SuggestDiffUtil : DiffUtil.ItemCallback<Song>() {
    override fun areItemsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem.title == newItem.title && oldItem.image == newItem.image && oldItem.artistId == newItem.artistId
    }

    override fun areContentsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem == newItem
    }

}