package com.example.baseproject.navigation

import android.os.Bundle
import com.example.baseproject.R
import com.example.core.navigationComponent.BaseNavigatorImpl
import com.example.setting.DemoNavigation
import javax.inject.Inject

class AppNavigatorImpl @Inject constructor() : BaseNavigatorImpl(),
    AppNavigation, DemoNavigation {

    override fun openSplashToHomeScreen(bundle: Bundle?) {
        openScreen(R.id.action_splashFragment_to_homeFragment, bundle)
    }

    override fun openHomePageToAlbum(bundle: Bundle?) {
        openScreen(R.id.action_homepage_to_album, bundle)
    }

    override fun openHomePageArtist(bundle: Bundle?) {
        openScreen(R.id.action_homepage_to_artist, bundle)
    }


    override fun openHomeToGenres(bundle: Bundle?) {
        openScreen(R.id.action_homepage_to_genrees, bundle)
    }

    override fun openUserToLocal(bundle: Bundle?) {
        openScreen(R.id.action_fragmentUser_to_fragmentLocalMusic, bundle)
    }

    override fun openHomeToPlaylist(bundle: Bundle?) {
        openScreen(R.id.action_fragmentUser_to_fragmentPlayList, bundle)
    }
    override fun openPlay(bundle: Bundle?) {
        openScreen(R.id.action_to_play, bundle)
    }

    override fun openHomeToPlay(bundle: Bundle?) {
        openScreen(R.id.action_home_to_play, bundle)
    }

    override fun openToLogin(bundle: Bundle?) {
        openScreen(R.id.action_to_login, bundle)
    }

    override fun openToRegister(bundle: Bundle?) {
        openScreen(R.id.action_to_register)
    }

    override fun openRegisterToHome(bundle: Bundle?) {
        openScreen(R.id.action_register_to_home, bundle)
    }

    override fun openLoginToHome(bundle: Bundle?) {
        openScreen(R.id.action_login_to_home, bundle)
    }

    override fun openFavourite(bundle: Bundle?) {
        openScreen(R.id.action_home_to_fav, bundle)
    }

    override fun registerToLogin(bundle: Bundle?) {
        openScreen(R.id.action_register_to_login, bundle)
    }
}