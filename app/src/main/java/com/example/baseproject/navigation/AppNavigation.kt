package com.example.baseproject.navigation

import android.os.Bundle
import com.example.core.navigationComponent.BaseNavigator

interface AppNavigation : BaseNavigator {

    fun openSplashToHomeScreen(bundle: Bundle? = null)

    fun openHomePageToAlbum(bundle: Bundle? = null)

    fun openHomePageArtist(bundle: Bundle? = null)

    fun openHomeToGenres(bundle: Bundle? = null)

    fun openUserToLocal(bundle: Bundle? = null)

    fun openHomeToPlaylist(bundle: Bundle? = null)

    fun openPlay(bundle: Bundle? = null)

    fun openHomeToPlay(bundle: Bundle? = null)

    fun openToLogin(bundle: Bundle? = null)

    fun openToRegister(bundle: Bundle? = null)

    fun openRegisterToHome(bundle: Bundle? = null)

    fun openLoginToHome(bundle: Bundle? = null)

    fun openFavourite(bundle: Bundle?? = null)

    fun registerToLogin(bundle: Bundle? = null)
}