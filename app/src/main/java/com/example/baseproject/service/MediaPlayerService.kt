package com.example.baseproject.service

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.*
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.example.baseproject.BaseApplication.Companion.CHANNEL_ID
import com.example.baseproject.R
import com.example.baseproject.container.MainActivity
import com.example.core.model.MediaPlayerSong
import com.example.core.utils.Constants
import android.os.CountDownTimer
import android.util.Log
import com.example.baseproject.Ultils.CacheMusic
import com.example.core.model.CacheMusicObject
import com.example.core.model.Time
import com.example.core.utils.isNetworkConnected
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.Runnable
import java.net.URL
import javax.inject.Inject


@AndroidEntryPoint
class MediaPlayerService @Inject constructor(): Service() {
    private var mediaPlayer: MediaPlayer? = null

    private val NUM_BYTES_NEEDED_FOR_MY_APP = 1024 * 1024 * 100L

    companion object {
        const val ACTION_PAUSE = 1
        const val ACTION_RESUME = 2
        const val ACTION_CLOSE = 3
        const val ACTION_NEXT = 4
        const val ACTION_REWIND = 5
    }
    private val handlerException = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }
    @Inject
    lateinit var cacheMusic: CacheMusic

    inner class MyBinder: Binder(){
        fun getMediaPlayerService(): MediaPlayerService = this@MediaPlayerService
    }

    private val binder = MyBinder()
    var isPlaying = false
    private var song: ArrayList<MediaPlayerSong>? = ArrayList()
    private var pos: Int = 0
    var isRepeat = false
    var isRandom = false
    var isAlarm = false
    var nameSong = ""
    var songCurrent: MediaPlayerSong? = null
    private var timeName: String? = ""
    private var countDownTimer: CountDownTimer? = null
    var first = true

    val handler = Handler(Looper.getMainLooper())
    private var runnable = object : Runnable {
        override fun run() {
            val intent = Intent(Constants.Bundle.SEND_DURATION_TO_FRAGMENT)
            intent.putExtra(Constants.Bundle.DURATION, mediaPlayer?.currentPosition)
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
            sendNotification(song?.get(pos))
            handler.postDelayed(this, 1000)
        }
    }

    val handlerHome = Handler(Looper.getMainLooper())
    private var runnableHome = object : Runnable {
        override fun run() {
            sendDataToHome(song?.get(pos))
            handlerHome.postDelayed(this, 1000)
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        return super.onUnbind(intent)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val bundle = intent?.extras
        if (bundle != null) {
            if (first){
                song = bundle.get(Constants.MediaPlayer.PUT_DATA_TO_SERVICE) as ArrayList<MediaPlayerSong>?
                pos = bundle.getInt(Constants.MediaPlayer.PUT_POSITION)
                startMusic(song?.get(pos))
                sendNotification(song?.get(pos))
                first = false
            }
        }
        handleActionMusic(intent?.getIntExtra(Constants.MediaPlayer.ACTION_MUSIC_RECEIVER, 0))
        return START_NOT_STICKY
    }

    @SuppressLint("SdCardPath")
    private fun startMusic(song: MediaPlayerSong?) {
        nameSong = song!!.name!!
        songCurrent = song
        var isCache = false
        mediaPlayer?.release()
        mediaPlayer = null
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer().apply {
                setAudioAttributes(
                    AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .build()
                )
                if (song.source?.startsWith("content://") == true) {
                    val uri: Uri = Uri.parse(song.source)
                    setDataSource(applicationContext, uri)
                    prepare()
                }else {
                    var path = ""
                    if (cacheMusic.readAllSong().size > 0) {
                        for (i in cacheMusic.readAllSong().indices) {
                            if (cacheMusic.readAllSong()[i].title.equals(song.name)) {
                                path = cacheMusic.readAllSong()[i].source.toString()
                                isCache = true
                                break
                            }
                        }
                        if (isCache) {
                            setDataSource(path)
                            prepare()
                        } else {
                            if (isNetworkConnected()) {
                                isCache = false
                                setDataSource(song.source)
                                prepare()
                                var count = 0
                                for (i in cacheMusic.readAllSong().indices){
                                    val file = cacheMusic.readAllSong()[i].source?.let { File(it) }
                                    count += file?.length()!!.toInt()
                                }
                                if (count >= NUM_BYTES_NEEDED_FOR_MY_APP) {
                                    cacheMusic.readAllSong()[0].source?.let {
                                        File(
                                            it
                                        )
                                    }?.delete()
                                    cacheMusic.deleteSong(cacheMusic.readAllSong()[0].id.toString())
                                }
                                CoroutineScope(Dispatchers.IO + SupervisorJob() + handlerException).launch {
                                    cacheMusic(song)
                                }
                            }

                        }
                    } else {
                        setDataSource(song.source)
                        prepare()
                        var count = 0
                        for (i in cacheMusic.readAllSong().indices){
                            val file = cacheMusic.readAllSong()[i].source?.let { File(it) }
                            count += file?.length()?.toInt()!!
                        }
                        if (count >= NUM_BYTES_NEEDED_FOR_MY_APP) {
                            cacheMusic.readAllSong()[0].source?.let { File(it) }?.delete()
                            cacheMusic.deleteSong(cacheMusic.readAllSong()[0].id.toString())
                        }
                        CoroutineScope(Dispatchers.IO + SupervisorJob() + handlerException).launch {
                            cacheMusic(song)
                        }
                    }
                }
                setOnPreparedListener{
                    start()
                }
                setOnCompletionListener {
                    try {
                        nextSong()
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
            }
            sendDataToFragment(song)
            handler.postDelayed(runnable, 1000)
            handlerHome.postDelayed(runnableHome, 1000)
            isPlaying = true
        }
    }

    private fun cacheMusic(song: MediaPlayerSong?){
        val url = URL(song?.source)
        val connection = url.openConnection()
        connection.connect()
        val inputStream = BufferedInputStream(url.openStream())
        val filename = song?.name
        val cacheFile = filename?.let { File(application.cacheDir, it) }
        val outputStream = FileOutputStream(cacheFile!!.path)
        val data = ByteArray(1024)
        var count = inputStream.read(data)
        var total = count
        while (count != -1) {
            outputStream.write(data, 0, count)
            count = inputStream.read(data)
            total += count
        }
        outputStream.flush()
        outputStream.close()
        inputStream.close()
        cacheMusic.insertSong(
            CacheMusicObject(
                null,
                cacheFile.path,
                song.name
            )
        )
    }

    private fun nextSong() {
        if (isNetworkConnected()){
            if (isRepeat){
                Log.d("ahihi", "isRepeat")
                sendDataToFragment(song?.get(pos))
                sendNotification(song?.get(pos))
                handler.removeCallbacks(runnable)
                handlerHome.removeCallbacks(runnableHome)
                startMusic(song?.get(pos))
            }else if(isRandom){
                Log.d("ahihi", "isRandom")
                val rands = (0 until song?.size!! - 1).random()
                sendDataToFragment(song?.get(rands))
                sendNotification(song?.get(rands))
                handler.removeCallbacks(runnable)
                handlerHome.removeCallbacks(runnableHome)
                startMusic(song?.get(rands))
            }
            else{
                Log.d("ahihi", "play")
                if (pos < song?.size!!-1){
                    pos++
                    sendDataToFragment(song?.get(pos))
                    sendNotification(song?.get(pos))
                    handler.removeCallbacks(runnable)
                    handlerHome.removeCallbacks(runnableHome)
                    startMusic(song?.get(pos))
                }else{
                    pos = 0
                    sendDataToFragment(song?.get(pos))
                    sendNotification(song?.get(pos))
                    handler.removeCallbacks(runnable)
                    handlerHome.removeCallbacks(runnableHome)
                    startMusic(song?.get(pos))
                }
            }
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag", "RemoteViewLayout")
    private fun sendNotification(songNotify: MediaPlayerSong?) {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        try {
            val bitmap: Bitmap = Glide
                .with(this)
                .asBitmap()
                .load(songNotify?.image)
                .submit()
                .get()
            val remoteViews = RemoteViews(packageName, R.layout.custom_media_player_noti)
            remoteViews.setTextViewText(R.id.txt_name_song_notification, songNotify?.name)
            remoteViews.setTextViewText(R.id.txt_name_artist_notification, songNotify?.artist)
            remoteViews.setImageViewBitmap(R.id.image_song_notification, bitmap)
            remoteViews.setImageViewResource(
                R.id.btn_next,
                R.drawable.ic_music_next_play_rounded_icon
            )
            remoteViews.setImageViewResource(R.id.btn_previous, R.drawable.ic_fast_rewind_icon)

            if (isPlaying) {
                remoteViews.setOnClickPendingIntent(
                    R.id.btn_play_or_pause,
                    getPendingIntent(this, ACTION_PAUSE)
                )
                remoteViews.setImageViewResource(R.id.btn_play_or_pause, R.drawable.ic_pause_icon)
            } else {
                remoteViews.setOnClickPendingIntent(
                    R.id.btn_play_or_pause,
                    getPendingIntent(this, ACTION_RESUME)
                )
                remoteViews.setImageViewResource(
                    R.id.btn_play_or_pause,
                    R.drawable.ic_play_icon
                )
            }
            remoteViews.setOnClickPendingIntent(
                R.id.btn_next,
                getPendingIntent(this, ACTION_NEXT)
            )
            remoteViews.setOnClickPendingIntent(
                R.id.btn_previous,
                getPendingIntent(this, ACTION_REWIND)
            )
            remoteViews.setOnClickPendingIntent(
                R.id.btn_close,
                getPendingIntent(this, ACTION_CLOSE)
            )

            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .setCustomContentView(remoteViews)
                .setSound(null)
                .build()

            startForeground(1, notification)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun handleActionMusic(action: Int?) {
        when (action) {
            ACTION_PAUSE -> {
                pauseMusic()
            }
            ACTION_RESUME -> {
                resumeMusic()
            }
            ACTION_CLOSE -> {
                stopServiceFromNotification()
                isPlaying = false
                closeMusic()
                stopForeground(true)
            }
            ACTION_NEXT -> {
                nextMusic()
            }
            ACTION_REWIND -> {
                previousMusic()
            }
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun getPendingIntent(context: Context, action: Int): PendingIntent {
        val intent = Intent(this, MediaPlayerReceiver::class.java)
        intent.putExtra(Constants.MediaPlayer.ACTION_MUSIC, action)

        return PendingIntent.getBroadcast(
            context.applicationContext, action, intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun sendDataToFragment(song: MediaPlayerSong?) {
        val intent = Intent(Constants.Bundle.SEND_DATA_TO_FRAGMENT)
        val bundle = Bundle()
        if (song != null) {
            nameSong = song.name.toString()
        }
        songCurrent = song
        bundle.putSerializable(Constants.Bundle.SONG, song)
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }
    private fun sendDataToHome(song: MediaPlayerSong?) {
        val intent = Intent(Constants.Bundle.SEND_DATA_TO_HOME)
        val bundle = Bundle()
        bundle.putSerializable(Constants.Bundle.SONG, song)
        intent.putExtras(bundle)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }

    fun pauseMusic(){
        if (mediaPlayer != null && isPlaying) {
            mediaPlayer!!.pause()
            isPlaying = false
            sendNotification(song?.get(pos))
        }
    }
    fun resumeMusic(){
        if (mediaPlayer != null && !isPlaying) {
            mediaPlayer!!.start()
            isPlaying = true
            sendNotification(song?.get(pos))
        }
    }

    fun nextMusic(){
        isPlaying = true
        if (isRandom){
            val rands = (0 until song?.size!! - 1).random()
            pos = rands
            sendDataToFragment(song?.get(rands))
            sendNotification(song?.get(rands))
            handler.removeCallbacks(runnable)
            handlerHome.removeCallbacks(runnableHome)
            startMusic(song?.get(rands))
        }else {
            if (pos < song?.size!!-1){
                pos++
                sendDataToFragment(song?.get(pos))
                sendNotification(song?.get(pos))
                handler.removeCallbacks(runnable)
                handlerHome.removeCallbacks(runnableHome)
                startMusic(song?.get(pos))
            } else{
                pos = 0
                sendDataToFragment(song?.get(pos))
                sendNotification(song?.get(pos))
                handler.removeCallbacks(runnable)
                handlerHome.removeCallbacks(runnableHome)
                startMusic(song?.get(pos))
            }
        }
    }

    fun previousMusic(){
        if (pos == 0){
            pos = song?.size!! - 1
            sendDataToFragment(song?.get(pos))
            sendNotification(song?.get(pos))
            handler.removeCallbacks(runnable)
            handlerHome.removeCallbacks(runnableHome)
            startMusic(song?.get(pos))
        }else{
            pos--
            sendDataToFragment(song?.get(pos))
            sendNotification(song?.get(pos))
            handler.removeCallbacks(runnable)
            handlerHome.removeCallbacks(runnableHome)
            startMusic(song?.get(pos))
        }
    }

    fun repeatMusic(){
        isRepeat = !isRepeat
    }

    fun seekTo(duration: Int?){
        mediaPlayer?.seekTo(duration!!)
    }
    fun closeMusic(){
        mediaPlayer!!.release()
        mediaPlayer = null
        first = true
        stopSelf()
        stopForeground(true)
        handler.removeCallbacks(runnable)
        handlerHome.removeCallbacks(runnableHome)
    }
    fun randomMusic(){
        isRandom = !isRandom
    }

    private fun stopServiceFromNotification(){
        val intent = Intent(Constants.Bundle.STOP_SERVICE_FROM_NOTIFY)
        intent.putExtra(Constants.Bundle.STOP, true)
        intent.putExtra(Constants.Bundle.SONG, song)
        intent.putExtra(Constants.Bundle.POSITION, pos)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }

    fun cancelAlarm(){
        isAlarm = !isAlarm
        countDownTimer?.cancel()
    }
    fun alarmService(time: Time?){
        isAlarm = !isAlarm
        if (isAlarm){
            timeName = time?.name
            if (time != null) {
                countDownTimer = object : CountDownTimer((time.time!! * 1000).toLong(), 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                    }
                    override fun onFinish() {
                        stopServiceFromNotification()
                        closeMusic()
                    }
                }.start()
            }else {
                countDownTimer?.cancel()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mediaPlayer != null) {
            mediaPlayer!!.release()
            mediaPlayer = null
        }
        handler.removeCallbacks(runnable)
        handlerHome.removeCallbacks(runnableHome)
        countDownTimer?.cancel()
    }
}
