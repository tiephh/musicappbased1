package com.example.baseproject.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.core.utils.Constants

class MediaPlayerReceiver: BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        val actionMusic = p1?.getIntExtra(Constants.MediaPlayer.ACTION_MUSIC, 0)
        val intent = Intent(p0, MediaPlayerService::class.java)
        intent.putExtra(Constants.MediaPlayer.ACTION_MUSIC_RECEIVER, actionMusic)
        p0?.startService(intent)
    }
}