package com.example.core.utils.listenner


interface OnItemClick {

    fun onItemClick(position: Int)

}