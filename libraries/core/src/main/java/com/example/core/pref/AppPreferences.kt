package com.example.core.pref

import android.content.Context
import android.content.SharedPreferences
import com.example.core.model.MediaPlayerSong
import com.example.core.model.Users
import com.example.core.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

private const val PREF_PARAM_USER_INFO = "PREF_PARAM_USER_INFO"

@Singleton
class AppPreferences @Inject constructor(
    @ApplicationContext context: Context
) : RxPreferences {

    private var mPrefs: SharedPreferences = context.getSharedPreferences(
        Constants.PREF_FILE_NAME,
        Context.MODE_PRIVATE
    )

    override fun put(key: String, value: String) {
        val editor: SharedPreferences.Editor = mPrefs.edit()
        editor.putString(key, value)
        editor.apply()
    }

    override fun get(key: String): String? {
        return mPrefs.getString(key, "")
    }

    override fun clear() {
        val editor: SharedPreferences.Editor = mPrefs.edit()
        editor.clear()
        editor.apply()
    }

    override fun remove(key: String) {
        val editor: SharedPreferences.Editor = mPrefs.edit()
        editor.remove(key)
        editor.apply()
    }

    override fun getToken() = get(PREF_PARAM_USER_INFO)

    override fun setUserToken(userToken: String) = put(PREF_PARAM_USER_INFO, userToken)

    override fun logout() {
        remove(PREF_PARAM_USER_INFO)
    }

    override fun setDataUsers(users: Users) {
        mPrefs.edit().putString(
            Constants.PREF_KEY_CURRENT_USER,
            Gson().toJson(users)
        ).apply()
    }

    override fun getDataUser(): Users {
        val json = mPrefs.getString(
            Constants.PREF_KEY_CURRENT_USER,
            ""
        )
        return Gson().fromJson(json, Users::class.java)
    }

    override fun setSongFirebase(song: MediaPlayerSong) {
        FirebaseDatabase.getInstance().getReference(Constants.Firebase.FAVOURITE)
            .child(getFirebaseUid()).push().setValue(song)
    }

    override fun getFirebaseUid(): String {
        return FirebaseAuth.getInstance().currentUser?.uid.toString()
    }

    override fun getDatabaseReference(path: String?): DatabaseReference {
        return FirebaseDatabase.getInstance().getReference(path.toString())
    }

}