package com.example.core.model

import java.io.Serializable

data class MediaPlayerSong(
    var id: Int? = null,
    var name: String? = "",
    var image: String? = "",
    var artist: String? = "",
    var source: String? = "",
    var duration: Int? = null
) : Serializable