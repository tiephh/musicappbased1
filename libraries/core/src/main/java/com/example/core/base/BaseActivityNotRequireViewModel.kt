package com.example.core.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.core.R
import com.example.core.utils.Constants
import com.example.core.utils.dialog.BaseDialog
import com.example.core.utils.dialog.LoadingDialog
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.*


abstract class BaseActivityNotRequireViewModel<BD : ViewDataBinding> : AppCompatActivity() {

    private var _binding: BD? = null
    protected val binding: BD get() = _binding!!

    private var lastTimeClick: Long = 0

    @get: LayoutRes
    abstract val layoutId: Int

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = DataBindingUtil.setContentView(WeakReference(this).get()!!, layoutId)
        _binding?.lifecycleOwner = this

    }

    override fun onDestroy() {
        _binding?.unbind()
        _binding = null
        super.onDestroy()
    }

    fun showLoading() {
        LoadingDialog.getInstance(this)?.show()
    }

    fun hiddenLoading() {
        LoadingDialog.getInstance(this)?.hidden()
    }

    override fun attachBaseContext(newBase: Context?) {
        val sharedPref = newBase?.getSharedPreferences(Constants.PREF_LANGUAGE, Context.MODE_PRIVATE)
        val lang = sharedPref?.getString("language1","")
        if (lang.equals("")){
            val current = Locale.getDefault().language
            with(sharedPref!!.edit()) {
                putString("language1", current)
                apply()
            }
            val locale = Locale(current)
            Locale.setDefault(locale)
            val configuration: Configuration? = newBase?.resources?.configuration
            configuration?.setLocale(locale)
            configuration?.setLayoutDirection(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                if (configuration != null) {
                    newBase.createConfigurationContext(configuration)
                }
            } else {
                resources.updateConfiguration(configuration, resources.displayMetrics)
            }
        }else{
            val sharedPref = newBase?.getSharedPreferences(Constants.PREF_LANGUAGE, Context.MODE_PRIVATE)
            val myLang = sharedPref?.getString("language1","")
            val locale = Locale(myLang.toString())
            Locale.setDefault(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                val configuration: Configuration? = newBase?.resources?.configuration
                configuration?.setLocale(locale)
                configuration?.setLayoutDirection(locale)
                if (configuration != null) {
                    newBase.createConfigurationContext(configuration)
                }
            } else {
                val res: Resources? = newBase?.resources
                val locale = Locale(myLang)
                Locale.setDefault(locale)
                val dm: DisplayMetrics? = res?.displayMetrics
                val config: Configuration = res!!.configuration
                resources.updateConfiguration(config, dm)
            }
        }
        super.attachBaseContext(newBase)
    }

    //click able
    val isDoubleClick: Boolean
        get() {
            val timeNow = SystemClock.elapsedRealtime()
            if (timeNow - lastTimeClick >= Constants.DURATION_TIME_CLICKABLE) {
                //click able
                lastTimeClick = timeNow
                return false
            }
            return true
        }


    /**
     * Close SoftKeyboard when user click out of EditText
     */
    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Timber.d("onBackPressed in activity")
    }

    private fun showAlertDialog(message: String) {
        BaseDialog(this)
            .setMessage(message)
            .setPositiveButton(R.string.ok, null)
            .show()
    }
}