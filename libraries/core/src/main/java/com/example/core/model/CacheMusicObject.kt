package com.example.core.model

data class CacheMusicObject (
    var id: Int? = null,
    var source: String? = "",
    var title: String? = "")