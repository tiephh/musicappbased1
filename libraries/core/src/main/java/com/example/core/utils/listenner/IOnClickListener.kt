package com.example.core.utils.listenner

interface IOnClickListener {
    fun onItemClick(position: Int)
    fun insertItem(position: Int)
    fun insertFav(position: Int)
}