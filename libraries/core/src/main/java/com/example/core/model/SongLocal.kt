package com.example.core.model

data class SongLocal (
    var id: Long? = null,
    var songUri: String? = "",
    var title: String? = "",
    var album: String? = "",
    var albumId: Long? = null,
    var artist: String? = "",
    var artistId: Long? = null,
    var duration: Long? = null,
    var image: String? = ""
)