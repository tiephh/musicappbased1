package com.example.core.utils

import com.example.core.model.Time
import java.util.*
import kotlin.collections.ArrayList

object Constants {
    const val PREF_FILE_NAME = "Preferences"
    const val PREF_LANGUAGE = "pref"
    const val DEFAULT_TIMEOUT = 30
    const val DURATION_TIME_CLICKABLE = 500
    const val ALBUM_ID = "album_id"
    const val MY_READ_PERMISSION_CODE = 101
    const val PREF_KEY_CURRENT_USER = "shareReferenceUser"

    object NetworkRequestCode {
        const val REQUEST_CODE_200 = 200    //normal
        const val REQUEST_CODE_400 = 400    //parameter error
        const val REQUEST_CODE_401 = 401    //unauthorized error
        const val REQUEST_CODE_403 = 403
        const val REQUEST_CODE_404 = 404    //No data error
        const val REQUEST_CODE_500 = 500    //system error
    }

    object ApiComponents {
        const val BASE_URL = "https://google.com"
    }
    object MediaPlayer{
        const val ACTION_MUSIC = "ACTION_MUSIC"
        const val ACTION_MUSIC_RECEIVER = "ACTION_MUSIC_RECEIVER"
        const val SONG_MEDIA_PLAYER = "song_media_player"
        const val PUT_DATA_TO_SERVICE = "data_song_to_service"
        const val PUT_POSITION = "position"
        const val SEEKBAR_PROCESS = "seekbar"
    }
    object Object{
        const val ALBUM = "album"
        const val ARTIST = "artist"
        const val SUGGEST = "suggest"
        const val GENREES = "gemrees"
    }

    object Section{
        const val SECTION_VIEWPAGER = "view_pager"
        const val SECTION_ALBUM = "section_album"
        const val SECTION_ARTIST = "section_artist"
        const val SECTION_SUGGEST = "section_suggest"
        const val SECTION_GENREES = "section_genrees"
    }

    object RoomDataBase{
        const val SOURCE = "source"
        const val IMAGE = "image"
        const val ID_SONG = "id_song"
        const val TITLE = "title"
        const val ALBUM_ID = "albumId"
        const val ARTIST_ID = "artistId"
        const val GENREES_ID = "genreId"
        const val DURATION = "duration"
        const val TABLE_NAME = "playListDefault"
        const val ID = "id"
        const val ID_FAVOURITE = "id_fav"
        const val TABLE_CACHE = "cache"
        const val ARTIST_NAME = "name"
        const val ARTIST_IMAGE = "image"
        const val ARTIST_FAVOURITE_ID = "id"
        const val IS_FAVOURITE = "favourite"
        const val TABLE_FAVOURITE = "favourite"
    }

    object TimeList{
        var time: ArrayList<Time> = Arrays.asList(
            Time("8 tiếng", 480),
            Time("4 tiếng", 240),
            Time("1 tiếng", 60),
            Time("30 phút", 30),
            Time("15 phút", 15),
            ) as ArrayList<Time>
    }

    object Bundle{
        const val DATA_PLAYING = "dataIsPlaying"
        const val SEND_DATA_TO_HOME = "send_data_to_home"
        const val SEND_DATA_TO_FRAGMENT = "send_data_to_fragment"
        const val SEND_DURATION_TO_FRAGMENT = "send_duration_to_fragment"
        const val STOP_SERVICE_FROM_NOTIFY = "stop_service_from_notification"
        const val SONG = "song"
        const val STOP = "stop"
        const val POSITION = "position"
        const val DURATION = "duration"
    }


    object Firebase{
        const val ID = "id"
        const val  USERNAME = "username"
        const val IMAGE = "image"
        const val DEFAULT = "default"
        const val USER = "user"
        const val FAVOURITE = "favourite"
    }
}
