package com.example.core.model

data class SectionHome(
    var listAlbum: ArrayList<Album>? = null,
    var listArtist: ArrayList<FavouriteArtist>? = null,
    var listSuggest: ArrayList<Song>? = null,
    var listGenrees: ArrayList<Genrees>? = null,
    var section: String? = ""
)
