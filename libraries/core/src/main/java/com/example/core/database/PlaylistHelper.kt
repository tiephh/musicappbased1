package com.example.core.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.example.core.model.Song
import com.example.core.utils.Constants
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class PlaylistHelper @Inject constructor(@ApplicationContext val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    @Throws(SQLiteConstraintException::class)
    fun insertSong(song: Song): Boolean {
        val db = writableDatabase

        val values = ContentValues()
        values.putNull(Constants.RoomDataBase.ID)
        values.put(Constants.RoomDataBase.SOURCE, song.source)
        values.put(Constants.RoomDataBase.IMAGE, song.image)
        values.put(Constants.RoomDataBase.ID_SONG, song.id)
        values.put(Constants.RoomDataBase.TITLE, song.title)
        values.put(Constants.RoomDataBase.ALBUM_ID, song.albumId)
        values.put(Constants.RoomDataBase.ARTIST_ID, song.artistId)
        values.put(Constants.RoomDataBase.GENREES_ID, song.genreId)
        values.put(Constants.RoomDataBase.DURATION, song.duration)

        db.insert(Constants.RoomDataBase.TABLE_NAME, null, values)

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteSong(id: String): Boolean {
        val db = writableDatabase
        val selection = Constants.RoomDataBase.ID + " LIKE ?"
        val selectionArgs = arrayOf(id)
        db.delete(Constants.RoomDataBase.TABLE_NAME, selection, selectionArgs)

        return true
    }

    fun readSong(id: String): ArrayList<Song> {
        val songList = ArrayList<Song>()
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + Constants.RoomDataBase.TABLE_NAME + " WHERE " + Constants.RoomDataBase.ID + "='" + id + "'", null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }

        var source: String
        var image: String
        var idSong: Int
        var title: String
        var albumId: Int
        var artistId: Int
        var genreesId: Int
        var duration: Int
        if (cursor!!.moveToFirst()) {
            while (cursor.isAfterLast == false) {
                source = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.SOURCE))
                image = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.IMAGE))
                idSong = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ID_SONG))
                title = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.TITLE))
                albumId = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ALBUM_ID))
                artistId = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ARTIST_ID))
                genreesId = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.GENREES_ID))
                duration = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.DURATION))

                songList.add(Song(source, image, idSong, title, albumId, artistId, genreesId, duration))
                cursor.moveToNext()
            }
        }
        return songList
    }

    fun readAllSong(): ArrayList<Song> {
        val song = ArrayList<Song>()
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + Constants.RoomDataBase.TABLE_NAME, null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }

        var source: String
        var image: String
        var idSong: Int
        var title: String
        var albumId: Int
        var artistId: Int
        var genreesId: Int
        var duration: Int
        if (cursor!!.moveToFirst()) {
            while (cursor.isAfterLast == false) {
                source = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.SOURCE))
                image = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.IMAGE))
                idSong = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ID))
                title = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.TITLE))
                albumId = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ALBUM_ID))
                artistId = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ARTIST_ID))
                genreesId = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.GENREES_ID))
                duration = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.DURATION))

                song.add(Song(source, image, idSong, title, albumId, artistId, genreesId, duration))
                cursor.moveToNext()
            }
        }
        return song
    }

    companion object {
        val DATABASE_VERSION = 1
        val DATABASE_NAME = "FeedReader.db"

        private val SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Constants.RoomDataBase.TABLE_NAME + " (" + Constants.RoomDataBase.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    Constants.RoomDataBase.SOURCE + " NVARCHAR(200)," +
                    Constants.RoomDataBase.IMAGE + " NVARCHAR(200)," +
                    Constants.RoomDataBase.ID_SONG + " NVARCHAR(200)," +
                    Constants.RoomDataBase.TITLE + " NVARCHAR(200)," +
                    Constants.RoomDataBase.ALBUM_ID + " INTEGER(10)," +
                    Constants.RoomDataBase.ARTIST_ID + " INTEGER(10)," +
                    Constants.RoomDataBase.GENREES_ID + " INTEGER(10)," +
                    Constants.RoomDataBase.DURATION + " INTEGER(10))"

        private val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + Constants.RoomDataBase.TABLE_NAME
    }

}