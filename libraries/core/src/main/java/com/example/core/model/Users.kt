package com.example.core.model

data class Users(
    var id: String? = "",
    var username: String? = "",
    var image: String? = ""
)
