package com.example.core.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.example.core.model.FavouriteArtist
import com.example.core.utils.Constants
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class FavouriteHelper @Inject constructor(@ApplicationContext val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    @Throws(SQLiteConstraintException::class)
    fun insertFavourite(favourite: FavouriteArtist): Boolean {
        val db = writableDatabase

        val values = ContentValues()
        values.putNull(Constants.RoomDataBase.ARTIST_FAVOURITE_ID)
        values.put(Constants.RoomDataBase.ARTIST_NAME, favourite.name)
        values.put(Constants.RoomDataBase.ARTIST_IMAGE, favourite.image)
        values.put(Constants.RoomDataBase.IS_FAVOURITE, favourite.isFavourite)
        values.put(Constants.RoomDataBase.ID_FAVOURITE, favourite.idFav)

        db.insert(Constants.RoomDataBase.TABLE_FAVOURITE, null, values)

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteFavourite(id: String): Boolean {
        val db = writableDatabase
        val selection = Constants.RoomDataBase.ARTIST_FAVOURITE_ID + " LIKE ?"
        val selectionArgs = arrayOf(id)
        db.delete(Constants.RoomDataBase.TABLE_FAVOURITE, selection, selectionArgs)

        return true
    }

    fun readFavourite(id: String): ArrayList<FavouriteArtist> {
        val favouriteList = ArrayList<FavouriteArtist>()
        val db = writableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("select * from " + Constants.RoomDataBase.TABLE_FAVOURITE + " WHERE " + Constants.RoomDataBase.ARTIST_FAVOURITE_ID + "='" + id + "'", null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }

        var name: String
        var image: String
        var id: Int
        var isFavourite: Int
        var idFav: Int
        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                name = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.ARTIST_NAME))
                image = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.ARTIST_IMAGE))
                id = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ARTIST_FAVOURITE_ID))
                isFavourite = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.IS_FAVOURITE))
                idFav = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ID_FAVOURITE))
                favouriteList.add(FavouriteArtist(name, image, id, isFavourite, idFav))
                cursor.moveToNext()
            }
        }
        return favouriteList
    }

    fun readAllFavourite(): ArrayList<FavouriteArtist> {
        val favourite = ArrayList<FavouriteArtist>()
        val db = writableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("select * from " + Constants.RoomDataBase.TABLE_FAVOURITE, null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }

        var name: String
        var image: String
        var id: Int
        var isFavourite: Int
        var idFav: Int
        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                name = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.ARTIST_NAME))
                image = cursor.getString(cursor.getColumnIndex(Constants.RoomDataBase.ARTIST_IMAGE))
                id = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ARTIST_FAVOURITE_ID))
                isFavourite = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.IS_FAVOURITE))
                idFav = cursor.getInt(cursor.getColumnIndex(Constants.RoomDataBase.ID_FAVOURITE))
                favourite.add(FavouriteArtist(name, image, id, isFavourite, idFav))
                cursor.moveToNext()
            }
        }
        return favourite
    }

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "FeedReader.db"

        private const val SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Constants.RoomDataBase.TABLE_FAVOURITE + " (" + Constants.RoomDataBase.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    Constants.RoomDataBase.ARTIST_NAME + " NVARCHAR(200)," +
                    Constants.RoomDataBase.ARTIST_IMAGE + " NVARCHAR(200)," +
                    Constants.RoomDataBase.ID_FAVOURITE + " INTEGER(200)," +
                    Constants.RoomDataBase.IS_FAVOURITE + " INTEGER(200))"

        private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + Constants.RoomDataBase.TABLE_FAVOURITE
    }

}