package com.example.core.model

data class AlbumDetail(
    var image: String? = "",
    var singer: String? = "",
    var title: String? = "",
    var song:String? = "",
    var duration: Int? = null
)
