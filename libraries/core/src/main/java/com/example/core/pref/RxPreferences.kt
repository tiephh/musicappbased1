package com.example.core.pref

import com.example.core.model.MediaPlayerSong
import com.example.core.model.Users
import com.google.firebase.database.DatabaseReference
import javax.inject.Singleton

@Singleton
interface RxPreferences {
    fun put(key: String, value: String)

    fun get(key: String): String?

    fun clear()

    fun remove(key: String)

    fun getToken(): String?

    fun setUserToken(userToken: String)

    fun logout()

    fun setDataUsers(users: Users)

    fun getDataUser(): Users

    fun setSongFirebase(song: MediaPlayerSong)

    fun getFirebaseUid(): String

    fun getDatabaseReference(path: String?): DatabaseReference
}