package com.example.core.model

data class FavouriteArtist (
    var name: String? = "",
    var image: String? = "",
    var id: Int? = null,
    var isFavourite: Int? = null,
    var idFav: Int? = null
)